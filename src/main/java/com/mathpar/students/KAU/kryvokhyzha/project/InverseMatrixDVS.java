package com.mathpar.students.KAU.kryvokhyzha.project;

import com.mathpar.log.MpiLogger;
import com.mathpar.matrix.MatrixD;
import com.mathpar.matrix.MatrixS;
import com.mathpar.number.Element;
import com.mathpar.number.Ring;
import com.mathpar.parallel.dap.core.DispThread;
import com.mathpar.parallel.dap.core.Drop;
import com.mathpar.parallel.dap.multiply.MatrixD.MatrDMult4;
import com.mathpar.parallel.dap.multiply.MatrixD.MatrDMultStrassWin7;
import com.mathpar.parallel.dap.multiply.MatrixD.MatrDMultiplyAdd;
import com.mathpar.parallel.dap.multiply.Multiply;
import mpi.MPI;
import mpi.MPIException;

import java.io.FileWriter;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;


public class InverseMatrixDVS extends Drop {
    public static int leafSize = 16;
    private final static MpiLogger LOGGER = MpiLogger.getLogger(InverseMatrixDVS.class);
    private static int[][] _arcs = new int[][]{
            {1, 0, 0,    2, 1, 1,    3, 2, 0,    4, 1, 1,    4, 3, 2},      // Input #0
            {2, 0, 0,    3, 0, 1,    8, 0, 2},                              // Inv #1
            {7, 0, 0,    8, 0, 0},                                          // Mul #2
            {4, 0, 0,    6, 0, 1},                                          // Mul #3
            {5, 0, 0},                                                      // MulAdd #4
            {6, 0, 0,    7, 0, 1,    9, 0, 3},                              // Inv #5 -> output
            {8, 0, 1,    9, 0, 2},                                          // Mul #6 -> output
            {9, 0, 1},                                                      // Mul #7 -> output
            {9, 0, 0},                                                      // MulAdd #8 -> output
            {}                                                              // Output #9
    };

    public InverseMatrixDVS() {
        inData = new Element[1];
        outData = new Element[1];
        numberOfMainComponents = 1;
        arcs = _arcs;
        type = 99;
        resultForOutFunctionLength = 4;
        numberOfMainComponentsAtOutput = 5;
        inputDataLength = 1;
        number = cnum++;
    }

    @Override
    public ArrayList<Drop> doAmin() {
        ArrayList<Drop> amin = new ArrayList<>();
        amin.add(new InverseMatrixDVS());
        amin.add(new MatrDMultStrassWin7());
        amin.add(new MatrDMultStrassWin7());
        amin.add(new MatrDMultiplyVSAdd7());
        amin.add(new InverseMatrixDVS());
        amin.add(new MatrDMultStrassWin7());
        amin.add(new MatrDMultStrassWin7());
        amin.add(new MatrDMultiplyVSAdd7());

        /*amin.add(new InverseMatrixDVS());
        amin.add(new MatrDMult4());
        amin.add(new MatrDMult4());
        amin.add(new MatrDMultiplyAdd());
        amin.add(new InverseMatrixDVS());
        amin.add(new MatrDMult4());
        amin.add(new MatrDMult4());
        amin.add(new MatrDMultiplyAdd());*/

        amin.get(1).key = 1;

        return amin;
    }

    @Override
    public void sequentialCalc(Ring ring) {
        MatrixD temp = (MatrixD) inData[0];
        outData[0] = new MatrixD(new MatrixS(temp.M, ring).inverseInFractions(ring), ring, temp.fl);
    }

    @Override
    public MatrixD[] inputFunction(Element[] input, int key, Ring ring) {

        MatrixD ms = (MatrixD) input[0];
        MatrixD[] blocks = ms.splitTo4();

        MatrixD[] res = new MatrixD[4];
        res[0] = blocks[0].negate(ring);
        res[1] = blocks[1];
        res[2] = blocks[2];
        res[3] = blocks[3];

        return res;
    }

    @Override
    public Element[] outputFunction(Element[] input, Ring ring) {
        MatrixD[] resInv = new MatrixD[4];

        resInv[0] = ((MatrixD) input[0]).negate(ring);
        resInv[1] = ((MatrixD) input[1]).negate(ring);
        resInv[2] = (MatrixD) input[2];
        resInv[3] = (MatrixD) input[3];

        return new MatrixD[]{MatrixD.join(resInv)};
    }

    @Override
    public boolean isItLeaf() {
        return (((MatrixD) inData[0]).M.length <= leafSize);
    }

    @Override
    public void setLeafSize(int dataSize) {
        leafSize = dataSize;
    }

    public static void main(String[] args) throws MPIException, InterruptedException, ClassNotFoundException, IOException {
        MPI.Init(args);

        int rank = MPI.COMM_WORLD.getRank();
        int ranks_cnt = MPI.COMM_WORLD.getSize();

        final int randomSeed = 42;
        final int n = 256;
        final int dropType = 99;
        final int density = 100;
        final int n_runs = 3;

        Ring ring1 = new Ring("R[]");
        ring1.setAccuracy(100);
        ring1.setFLOATPOS(12);
        ring1.setMachineEpsilonR(90);

        Ring ring2 = new Ring("R64[]");
        ring2.setFLOATPOS(12);
        
        Random rand = new Random(randomSeed);

        List<Map<String, String>> resultList = null;

        resultList = densityTestRun(n_runs, rank, ranks_cnt, n, new int[] {30, 50, 75, 100}, rand, ring1, dropType, args);
        if (rank == 0) {
            createCSV("np" + ranks_cnt + "DensityTestsRingR.csv", resultList);
        }

        resultList = scalingTestRun(n_runs, rank, ranks_cnt, new int[] {64, 128, 256, 512}, density, rand, ring1, dropType, args);
        if (rank == 0) {
            createCSV("np" + ranks_cnt + "ScalingTestsRingR.csv", resultList);
        }

        resultList = densityTestRun(n_runs, rank, ranks_cnt, n, new int[] {30, 50, 75, 100}, rand, ring2, dropType, args);
        if (rank == 0) {
            createCSV("np" + ranks_cnt + "DensityTestsRingR64.csv", resultList);
        }

        resultList = scalingTestRun(n_runs, rank, ranks_cnt, new int[] {64, 128, 256, 512}, density, rand, ring2, dropType, args);
        if (rank == 0) {
            createCSV("np" + ranks_cnt + "ScalingTestsRingR64.csv", resultList);
        }

        MPI.Finalize();
    }

    private static Map<String, String> simpleRun(
            int rank, int ranks_cnt, int n, int density, Random rand, Ring ring, int dropType,
            String[] args
    ) throws MPIException, IOException, InterruptedException, ClassNotFoundException {
        long start = 0;
        final int coef = 4;
        final int leafSize = 64;//n > 32 ? (int) (n / coef) : 8;
        
        InverseMatrixDVS.leafSize = leafSize;
        MatrDMultStrassWin7.LeafSize = leafSize;
        MatrDMultiplyVSAdd7.LeafSize = leafSize;
        
        MatrixD a = new MatrixD(n, n, density, new int[] {5}, rand, ring);
        DispThread disp = new DispThread(1, args, ring);
        Map<String, String> resultMap = new HashMap<>();

        if (rank == 0) {
            LOGGER.info("Count of ranks: " + ranks_cnt);
            LOGGER.info("Leaf size: " + leafSize);
            LOGGER.info("Density: " + density);
            LOGGER.info("Size of input matrix A: " + n);

            System.out.println("Input matrix A:");
            showMatrix(a);

            start = System.currentTimeMillis();
        }

        disp.execute(dropType, 1, args, new MatrixD[]{a}, ring);

        if (rank == 0) {
            MatrixD result = (MatrixD) disp.getResult()[0];

            long timeElapsed = System.currentTimeMillis() - start;
            LOGGER.info("Time elapsed: " + timeElapsed);

            System.out.println("Result matrix A^-1:");
            showMatrix(result);

            System.out.println();
            LOGGER.info("Size of output matrix A^-1: " + result.M.length);
            LOGGER.info("Sum of elements of abs(A^-1): " + round(getSumOfElements(result, true), 8));

            MatrixD zeroMatrix = a.multiply8(result, ring).subtract(MatrixD.oneMatrixD(n, n, ring), ring);
            double check_value = round(getSumOfElements(zeroMatrix, false), 8);
            LOGGER.info("Sum of elements of ((A x A^-1) - I): " + check_value);

            resultMap.put("count_of_ranks", Integer.toString(ranks_cnt));
            resultMap.put("leaf_size", Integer.toString(leafSize));
            resultMap.put("density", Integer.toString(density));
            resultMap.put("size_of_matrix", Integer.toString(n));
            resultMap.put("time_elapsed", Long.toString(timeElapsed));
            resultMap.put("check_value", Double.toString(check_value));
        }

        disp.counter.DoneThread();
        disp.counter.thread.join();

        return resultMap;
    }

    private static List<Map<String, String>> simpleTestRun(
            int n_runs, int rank, int ranks_cnt, int n, int density, Random rand, Ring ring, int dropType,
            String[] args
    ) throws MPIException, IOException, InterruptedException, ClassNotFoundException {
        if(rank == 0) System.out.println("Start simple run test...\n");
        List<Map<String, String>> resultList = new ArrayList<>();

        for(int i = 0; i < n_runs; i++) {
            Map<String, String> resultMap = simpleRun(rank, ranks_cnt, n, density, rand, ring, dropType, args);
            if (rank == 0) {
                System.out.println("=======================================================");
                System.out.println("=======================================================");
                System.out.println();
                resultList.add(resultMap);
            }
        }

        return resultList;
    }

    private static List<Map<String, String>> densityTestRun(
            int n_runs, int rank, int ranks_cnt, int n, int[] densities, Random rand, Ring ring, int dropType,
            String[] args
    ) throws MPIException, IOException, InterruptedException, ClassNotFoundException {
        if(rank == 0) System.out.println("Start density test...\n");
        List<Map<String, String>> resultList = new ArrayList<>();

        for(int i = 0; i < n_runs; i++) {
            for (int density : densities) {
                Map<String, String> resultMap = simpleRun(rank, ranks_cnt, n, density, rand, ring, dropType, args);
                if (rank == 0) {
                    System.out.println("=======================================================");
                    System.out.println("=======================================================");
                    System.out.println();
                    resultList.add(resultMap);
                }
            }
        }

        return resultList;
    }

    private static List<Map<String, String>> scalingTestRun(
            int n_runs, int rank, int ranks_cnt, int[] n_sizes, int density, Random rand, Ring ring, int dropType,
            String[] args
    ) throws MPIException, IOException, InterruptedException, ClassNotFoundException {
        if(rank == 0) System.out.println("Start scaling test...\n");
        List<Map<String, String>> resultList = new ArrayList<>();

        for(int i = 0; i < n_runs; i++) {
            for (int n : n_sizes) {
                Map<String, String> resultMap = simpleRun(rank, ranks_cnt, n, density, rand, ring, dropType, args);
                if (rank == 0) {
                    System.out.println("=======================================================");
                    System.out.println("=======================================================");
                    System.out.println();
                    resultList.add(resultMap);
                }
            }
        }

        return resultList;
    }

    public static void createCSV(String path, List<Map<String, String>> list) throws IOException{
        List<String> headers = list.stream().flatMap(map -> map.keySet().stream()).distinct().collect(Collectors.toList());

        try(FileWriter writer= new FileWriter(path, true);){
            for (String string : headers) {
                writer.write(string);
                writer.write(",");
            }
            writer.write("\r\n");

            for (Map<String, String> lmap : list) {
                for (Map.Entry<String, String> string2 : lmap.entrySet()) {
                    writer.write(string2.getValue());
                    writer.write(",");
                }
                writer.write("\r\n");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static double getSumOfElements(MatrixD matrix, boolean applyABS) {
        double sum = 0.0;
        for (int i = 0; i < matrix.M.length; i++) {
            for (int j = 0; j < matrix.M.length; j++) {
                sum += applyABS ? Math.abs(matrix.M[i][j].doubleValue()) : matrix.M[i][j].doubleValue();
            }
        }
        return sum;
    }

    private static double round(double value, int places) {
        if (places < 0) throw new IllegalArgumentException();

        long factor = (long) Math.pow(10, places);
        value = value * factor;
        long tmp = Math.round(value);
        return (double) tmp / factor;
    }

    private static void showMatrix(MatrixD matrix) {
        if (matrix.M.length <= 16)
            System.out.println(matrix);
        else
            System.out.println(matrix.toString().substring(0, 100) + "...");
        System.out.println();
    }
}

/*
mvn compile -DskipTests
*/

/*
mpirun --hostfile hostfile -np 4 java -cp $HOME/IdeaProjects/DAP/target/classes:$HOME/.m2/repository/org/apache/logging/log4j/log4j-api/2.12.1/log4j-api-2.12.1.jar:$HOME/.m2/repository/org/apache/logging/log4j/log4j-core/2.12.1/log4j-core-2.12.1.jar com/mathpar/students/KAU/kryvokhyzha/project/InverseMatrixDVS
*/
