package com.mathpar.students.KAU.kryvokhyzha.project;

import com.mathpar.log.MpiLogger;
import com.mathpar.matrix.MatrixD;
import com.mathpar.number.Element;
import com.mathpar.number.Ring;
import com.mathpar.parallel.dap.core.Drop;
import com.mathpar.parallel.dap.multiply.MatrixD.MatrDMultStrassWin7;
import com.mathpar.parallel.dap.multiply.MatrixD.MatrDMultiplyAdd;

import java.util.ArrayList;


public class InverseMatrixDVS_slow extends Drop {
    private static int leafSize = 16;
    private final static MpiLogger LOGGER = MpiLogger.getLogger(InverseMatrixDVS_slow.class);
    private static int[][] _arcs = new int[][]{
            {1, 3, 0, 2, 0, 0, 3, 1, 0, 4, 5, 1, 4, 0, 2, 6, 2, 1, 8, 2, 0, 9, 4, 1, 9, 3, 2, 11, 1, 1}, // Input #0
            {3, 0, 1, 6, 0, 0}, // Inv #1
            {8, 0, 1, 11, 0, 0}, // Inv #2
            {4, 0, 0}, // Mul #3
            {5, 0, 0}, // MulAdd #4
            {7, 0, 1, 13, 0, 0}, // Inv #5 -> output
            {7, 0, 0}, // Mul #6
            {13, 0, 2}, // Mul #7 -> output
            {9, 0, 0}, // Mul #8
            {10, 0, 0}, // MulAdd #9
            {12, 0, 1, 13, 0, 3}, // Inv #10 -> output
            {12, 0, 0}, // Mul #11
            {13, 0, 1}, // Mul #12 -> output
            {} // Output #13
    };

    public InverseMatrixDVS_slow() {
        inData = new Element[1];
        outData = new Element[1];
        numberOfMainComponents = 1;
        arcs = _arcs;
        type = 99;
        resultForOutFunctionLength = 4;
        numberOfMainComponentsAtOutput = 10;
        inputDataLength = 1;
        number = cnum++;
    }

    @Override
    public ArrayList<Drop> doAmin() {
        ArrayList<Drop> amin = new ArrayList<>();
        amin.add(new InverseMatrixDVS_slow());
        amin.add(new InverseMatrixDVS_slow());
        amin.add(new MatrDMultStrassWin7());
        amin.add(new MatrDMultiplyAdd());
        amin.add(new InverseMatrixDVS_slow());
        amin.add(new MatrDMultStrassWin7());
        amin.add(new MatrDMultStrassWin7());
        amin.add(new MatrDMultStrassWin7());
        amin.add(new MatrDMultiplyAdd());
        amin.add(new InverseMatrixDVS_slow());
        amin.add(new MatrDMultStrassWin7());
        amin.add(new MatrDMultStrassWin7());

        return amin;
    }

    @Override
    public void sequentialCalc(Ring ring) {
        outData[0] = ((MatrixD) inData[0]).inverse(ring);
    }

    @Override
    public MatrixD[] inputFunction(Element[] input, int key, Ring ring) {

        MatrixD ms = (MatrixD) input[0];
        MatrixD[] blocks = ms.splitTo4();

        MatrixD[] res = new MatrixD[6];
        res[0] = blocks[0];
        res[1] = blocks[1];
        res[2] = blocks[2];
        res[3] = blocks[3];
        res[4] = blocks[1].negate(ring);
        res[5] = blocks[2].negate(ring);

        return res;
    }

    @Override
    public Element[] outputFunction(Element[] input, Ring ring) {
        MatrixD[] resInv = new MatrixD[4];

        resInv[0] = (MatrixD) input[0];
        resInv[1] = ((MatrixD) input[1]).negate(ring);
        resInv[2] = ((MatrixD) input[2]).negate(ring);
        resInv[3] = (MatrixD) input[3];

        return new MatrixD[]{MatrixD.join(resInv)};
    }

    @Override
    public boolean isItLeaf() {
        return (((MatrixD) inData[0]).M.length <= leafSize);
    }

    @Override
    public void setLeafSize(int dataSize) {
        leafSize = dataSize;
    }
}
