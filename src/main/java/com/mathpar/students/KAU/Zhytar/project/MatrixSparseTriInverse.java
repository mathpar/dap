package com.mathpar.students.KAU.Zhytar.project;
import com.mathpar.log.MpiLogger;
import com.mathpar.matrix.MatrixD;
import com.mathpar.matrix.MatrixS;
import com.mathpar.number.Element;
import com.mathpar.number.Ring;
import com.mathpar.parallel.dap.core.DispThread;
import com.mathpar.parallel.dap.core.Drop;
import com.mathpar.parallel.dap.multiply.MatrixS.MatrSMult8;
import com.mathpar.parallel.dap.multiply.MatrixS.MatrSMultiplyAdd;
import mpi.MPI;
import mpi.MPIException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;

public class MatrixSparseTriInverse extends Drop {
    private final static MpiLogger LOGGER = MpiLogger.getLogger(MatrixSparseTriInverse.class);
    private static int leafSize = 32;

    private static int[][] _arcs = new int[][]{
            {1, 0, 0,    2, 1, 1,    3, 2, 0,    4, 1, 1,    4, 3, 2},      // Input #0
            {2, 0, 0,    3, 0, 1,    8, 0, 2},                              // Inv #1
            {7, 0, 0,    8, 0, 0},                                          // Mul #2
            {4, 0, 0,    6, 0, 1},                                          // Mul #3
            {5, 0, 0},                                                      // MulAdd #4
            {6, 0, 0,    7, 0, 1,    9, 0, 3},                              // Inv #5 -> output
            {8, 0, 1,    9, 0, 2},                                          // Mul #6 -> output
            {9, 0, 1},                                                      // Mul #7 -> output
            {9, 0, 0},                                                      // MulAdd #8 -> output
            {}                                                              // Output #9
    };

    public MatrixSparseTriInverse() {
        inData = new Element[1];
        outData = new Element[1];
        numberOfMainComponents = 1;
        numberOfMainComponentsAtOutput = 5;
        type = 228;
        resultForOutFunctionLength = 4;
        inputDataLength = 1;
        number = cnum++;
        arcs = _arcs;
    }

    @Override
    public ArrayList<Drop> doAmin() {
        ArrayList<Drop> amin = new ArrayList<>();
        amin.add(new MatrixSparseTriInverse());
        amin.add(new MatrSMult8());
        amin.add(new MatrSMult8());
        amin.add(new MatrSMultiplyAdd());
        amin.add(new MatrixSparseTriInverse());
        amin.add(new MatrSMult8());
        amin.add(new MatrSMult8());
        amin.add(new MatrSMultiplyAdd());
        return amin;
    }

    protected MatrixS inverse_sequential(MatrixS m, Ring r) {
        return m.inverseInFractions(r);
    }

    @Override
    public void sequentialCalc(Ring r) {
        MatrixS m = (MatrixS)inData[0];
        outData[0] = inverse_sequential(m, r);
    }

    @Override
    public MatrixS[] inputFunction(Element[] input, int key, Ring ring) {

        MatrixS ms = (MatrixS) input[0];
        MatrixS[] blocks = ms.split();

        MatrixS[] res = new MatrixS[4];
        res[0] = blocks[0].negate(ring);
        res[1] = blocks[1];
        res[2] = blocks[2];
        res[3] = blocks[3];

        return res;
    }

    @Override
    public Element[] outputFunction(Element[] input, Ring ring) {
        MatrixS[] resInv = new MatrixS[4];

        resInv[0] = ((MatrixS) input[0]).negate(ring);
        resInv[1] = ((MatrixS) input[1]).negate(ring);
        resInv[2] = (MatrixS) input[2];
        resInv[3] = (MatrixS) input[3];

        return new MatrixS[]{MatrixS.join(resInv)};
    }

    @Override
    public boolean isItLeaf() {
        MatrixS m = (MatrixS) inData[0];
        return  m.M.length <= leafSize;
    }

    @Override
    public void setLeafSize(int dataSize) {
        leafSize = dataSize;
    }

    public static void setLeafSizeStatic(int dataSize) {
        leafSize = dataSize;
    }

    public static long[][] getRandomMatrix(int size, double density) {
        Random random = new Random();
        long[][] m = new long[size][size];
        for (int i = 0; i < size; ++i) {
            for (int j = 0; j < size; ++j) {
                int number = (random.nextInt(50) + 1);
                if (random.nextDouble() > density) {
                    number = 0;
                }
                m[i][j] =  number;
            }
        }
        // ensure that main diagonal doesn't have zeros
        for (int i = 0; i < size; ++i) {
            if (m[i][i] == 0) {
                for (int j = 0; j < size; ++j) {
                    if (i != j && m[i][j] != 0) {
                        m[i][i] = m[i][j];
                        m[i][j] = 0;
                    }
                }
            }
        }
        return m;
    }

    public static long[][] getRandt(int size, double density) {
        Random random = new Random();
        long[][] m = new long[size][size];
        for (int i = 0; i < size; ++i) {
            for (int j = 0; j <= i; ++j) {
                int number = (random.nextInt(50) + 1);
                m[i][j] = number;
            }
        }
        return m;
    }

    static public void printMatrix(MatrixS m, Ring ring) {
        for (int i = 0; i < m.M.length; ++i) {
            for (int j = 0; j < m.M.length; ++j)
                System.out.printf("%3.4f ", m.getElement(i, j, ring).doubleValue());
            System.out.println(" ");
        }
        System.out.println(" ");
    }

    static private double getSum(MatrixS m, Ring ring) {
        double max = 0;
        for (int i = 0; i < m.M.length; ++i) {
            for (int j = 0; j < m.M.length; ++j)
                max = Math.max(max, Math.abs(m.getElement(i, j, ring).doubleValue()));
        }
        return max;
    }

    static private double getIdentity(int n) {
        MatrixS m = MatrixS.zeroMatrix(n);
        return 0.0;
    }

    public static void main(String[] args) throws MPIException, InterruptedException, ClassNotFoundException, IOException, Exception {
        MPI.Init(args);
        int rank = MPI.COMM_WORLD.getRank();
        double density = 0.15;
        int n = 128;
        long start = 0;

        if (rank == 0) {
            for (int i = 0; i < args.length; i += 2) {
                if (args[i].equals("-size")) {
                    n = Integer.parseInt(args[i+1]);
                } else if (args[i].equals("-leaf")) {
                    setLeafSizeStatic(Integer.parseInt(args[i+1]));
                } else if (args[i].equals("-density")) {
                    density = Double.parseDouble(args[i+1]);
                }
            }
            LOGGER.info("n = " + n + " leaf = " + leafSize + " density = " + density);
        }

        Ring ring = new Ring("R64[]");
//        ring.setAccuracy(100);
//        ring.setMachineEpsilonR(90);
        MatrixS res;
        DispThread disp = new DispThread(1, args, ring);
        Element[] init = {};

        MatrixS a = null;
        if (rank == 0) {
            a = new MatrixS(getRandt(n, density), ring);
            init = new MatrixS[] { a };
            if (a.M.length <= 16) {
                printMatrix(a, ring);
            }
            start = System.currentTimeMillis();
        }

        disp.execute(228, 1, args, init, ring);

        if (rank == 0) {
            long timeElapsed = System.currentTimeMillis() - start;
            LOGGER.info("time elapsed : " + timeElapsed);
        }

        disp.counter.DoneThread();
        disp.counter.thread.join();
        MPI.Finalize();
    }

}