package com.mathpar.students.KAU.goryslavets;

import com.mathpar.log.MpiLogger;
import com.mathpar.matrix.MatrixS;
import com.mathpar.number.Element;
import com.mathpar.number.NumberR;
import com.mathpar.number.NumberR64;
import com.mathpar.number.Ring;
import com.mathpar.parallel.dap.core.Drop;
import com.mathpar.parallel.dap.multiply.MatrixS.MatrSMultStrassWin7;
import com.mathpar.parallel.dap.multiply.MatrixS.MatrSMultiplyAdd;
import com.mathpar.parallel.dap.multiply.MultiplyAdd;

import com.mathpar.parallel.dap.core.DispThread;
import mpi.MPI;
import mpi.MPIException;

import java.io.FileWriter;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;


import java.util.ArrayList;

public class SparseWSMatrixInverse extends Drop {
    private static int leafSize = 8;
    private final static MpiLogger LOGGER = MpiLogger.getLogger(SparseWSMatrixInverse.class);
    private static int[][] _arcs = new int[][]{
            {1, 0, 0, 2, 2, 0, 3, 1, 1, 4, 2, 0, 4, 3, 2}, // 0. Input
            {2, 0, 1, 3, 0, 0, 8, 0, 2}, // 1. inv(A) = P1 -> [2, 3, 8]
            {7, 0, 1}, // 2. C*P1 = P2 -> [7]
            {4, 0, 1, 6, 0, 0, 8, 0, 0}, // 3. P1*B = P3 -> [4, 6, 8]
            {5, 0, 0}, // 4. C*P3 + (-D) = P4 -> [5]
            {6, 0, 1, 7, 0, 0, 9, 0, 3}, // 5. inv(P4) = P5 -> [6, 7, 9]
            {9, 0, 1}, // 6. P3*P5 = C12-> [9]
            {8, 0, 1, 9, 0, 2}, // 7. P5*P2 = (-)C21 -> [8, 9]
            {9, 0, 0}, // 8. P3*(-)C21 + P1 = C11 -> [9]
            {} // 9. Output
    };

    public SparseWSMatrixInverse() {
        inData = new Element[1];
        outData = new Element[1];
        numberOfMainComponents = 1;
        numberOfMainComponentsAtOutput = 5;
        type = 48;
        resultForOutFunctionLength = 4;
        inputDataLength = 1;
        number = cnum++;
        arcs = _arcs;
    }

    @Override
    public ArrayList<Drop> doAmin() {
        ArrayList<Drop> amin = new ArrayList<>();
        amin.add(new SparseWSMatrixInverse());      // 1
        amin.add(new MatrSMultStrassWin7());        // 2
        amin.add(new MatrSMultStrassWin7());        // 3
        amin.add(new MatrSMultiplyAdd());           // 4
        amin.add(new SparseWSMatrixInverse());      // 5
        amin.add(new MatrSMultStrassWin7());        // 6
        amin.add(new MatrSMultStrassWin7());        // 7
        amin.add(new MatrSMultiplyAdd());           // 8

        amin.get(6).key = 1; // return negated

        return amin;
    }

    @Override
    public void sequentialCalc(Ring ring) {
        outData[0] = ((MatrixS) inData[0]).inverse(ring);
    }

    @Override
    public MatrixS[] inputFunction(Element[] input, int key, Ring ring) {

        MatrixS ms = (MatrixS) input[0];
        MatrixS[] blocks = ms.split();

        MatrixS[] res = new MatrixS[4];
        res[0] = blocks[0];
        res[1] = blocks[1];
        res[2] = blocks[2];
        res[3] = blocks[3].negate(ring);

        return res;
    }

    @Override
    public Element[] outputFunction(Element[] input, Ring ring) {
        MatrixS[] resInv = new MatrixS[4];

        resInv[0] = (MatrixS) input[0];
        resInv[1] = (MatrixS) input[1];
        resInv[2] = ((MatrixS) input[2]).negate(ring);
        resInv[3] = ((MatrixS) input[3]).negate(ring);

        return new MatrixS[]{MatrixS.join(resInv)};
    }

    @Override
    public boolean isItLeaf() {
        return (((MatrixS) inData[0]).M.length <= leafSize);
    }

    @Override
    public void setLeafSize(int dataSize) {
        leafSize = dataSize;
    }



    public static void main(String[] args) throws MPIException, InterruptedException, ClassNotFoundException, IOException {
        MPI.Init(args);

        int rank = MPI.COMM_WORLD.getRank();
        int ranks_cnt = MPI.COMM_WORLD.getSize();

        final int randomSeed = 18;
        final int n = 64;
        final int dropType = 48;
        final int density = 100;
        final int n_runs = 1;

        // Ring ring1 = new Ring("R[]");
        // ring1.setAccuracy(100);
        // ring1.setFLOATPOS(12);
        // ring1.setMachineEpsilonR(90);

        Ring ring2 = new Ring("R64[]");
        ring2.setFLOATPOS(12);

        Random rand = new Random(randomSeed);

        List<Map<String, String>> resultList = null;

        /*resultList = densityTestRun(n_runs, rank, ranks_cnt, n, new int[] {50, 75, 100}, rand, ring1, dropType, args);
        if (rank == 0) {
            createCSV("SparseWS_np" + ranks_cnt + "DensityTestsRingR.csv", resultList);
        }

        resultList = scalingTestRun(n_runs, rank, ranks_cnt, new int[] {32, 64, 128}, density, rand, ring1, dropType, args);
        if (rank == 0) {
            createCSV("SparseWS_np" + ranks_cnt + "ScalingTestsRingR.csv", resultList);
        }*/

        resultList = densityTestRun(n_runs, rank, ranks_cnt, n, new int[] {10}, rand, ring2, dropType, args);
        if (rank == 0) {
            createCSV("SparseWS_np" + ranks_cnt + "DensityTestsRingR64.csv", resultList);
        }

        /*resultList = scalingTestRun(n_runs, rank, ranks_cnt, new int[] {32, 64, 128}, density, rand, ring2, dropType, args);
        if (rank == 0) {
            createCSV("SparseWS_np" + ranks_cnt + "ScalingTestsRingR64.csv", resultList);
        }*/

        MPI.Finalize();
    }

    private static Map<String, String> simpleRun(
            int rank, int ranks_cnt, int n, int density, Random rand, Ring ring, int dropType,
            String[] args
    ) throws MPIException, IOException, InterruptedException, ClassNotFoundException {
        long start = 0;
        final int leafSize = 8;

        SparseWSMatrixInverse.leafSize = leafSize;
        MatrSMultStrassWin7.LeafSize = leafSize;

        MatrixS a = new MatrixS(n, n, density, new int[] {3}, rand, NumberR.ONE, ring);

        // main diagonal shouldn't contain zeros
        for (int i = 0; i < a.col.length; ++i) {
            boolean diagElementIsZero = true;

            int j = 0;
            for (j = 0; j < a.col[i].length; ++j) {
                if (a.col[i][j] == i) {
                    diagElementIsZero = false;
                    break;
                }
            }

            if (diagElementIsZero) {
                a.col[i][0] = i;
            }
        }

        DispThread disp = new DispThread(1, args, ring);
        Map<String, String> resultMap = new HashMap<>();

        if (rank == 0) {
            LOGGER.info("Count of ranks: " + ranks_cnt);
            LOGGER.info("Leaf size: " + leafSize);
            LOGGER.info("Density: " + density);
            LOGGER.info("Size of input matrix A: " + n);

            // System.out.println("Input matrix A:");
            // System.out.println(a.print(ring));

            start = System.currentTimeMillis();
        }

        disp.execute(dropType, 0, args, new MatrixS[]{a}, ring);

        if (rank == 0) {
            MatrixS result = (MatrixS) disp.getResult()[0];

            long timeElapsed = System.currentTimeMillis() - start;
            LOGGER.info("Time elapsed: " + timeElapsed);

            // System.out.println("Result matrix A^-1:");
            // System.out.println(result.print(ring));

            // System.out.println();

            // LOGGER.info("Size of output matrix A^-1: " + result.M.length);
            // LOGGER.info("Sum of elements of abs(A^-1): " + round(getSumOfElements(result, true), 8));

            MatrixS zeroMatrix = a.multiply(result, ring).subtract(a.myOne(ring), ring);
            double check_value = round(getSumOfElements(zeroMatrix, false), 8);

            // LOGGER.info("Sum of elements of ((A x A^-1) - I): " + check_value);

            resultMap.put("count_of_ranks", Integer.toString(ranks_cnt));
            resultMap.put("leaf_size", Integer.toString(leafSize));
            resultMap.put("density", Integer.toString(density));
            resultMap.put("size_of_matrix", Integer.toString(n));
            resultMap.put("time_elapsed", Long.toString(timeElapsed));
            resultMap.put("check_value", Double.toString(check_value));
        }

        disp.counter.DoneThread();
        disp.counter.thread.join();

        return resultMap;
    }

    private static List<Map<String, String>> simpleTestRun(
            int n_runs, int rank, int ranks_cnt, int n, int density, Random rand, Ring ring, int dropType,
            String[] args
    ) throws MPIException, IOException, InterruptedException, ClassNotFoundException {
        if(rank == 0) System.out.println("Start simple run test...\n");
        List<Map<String, String>> resultList = new ArrayList<>();

        for(int i = 0; i < n_runs; i++) {
            Map<String, String> resultMap = simpleRun(rank, ranks_cnt, n, density, rand, ring, dropType, args);
            if (rank == 0) {
                System.out.println("=======================================================");
                System.out.println("=======================================================");
                System.out.println();
                resultList.add(resultMap);
            }
        }

        return resultList;
    }

    private static List<Map<String, String>> densityTestRun(
            int n_runs, int rank, int ranks_cnt, int n, int[] densities, Random rand, Ring ring, int dropType,
            String[] args
    ) throws MPIException, IOException, InterruptedException, ClassNotFoundException {
        if(rank == 0) System.out.println("Start density test...\n");
        List<Map<String, String>> resultList = new ArrayList<>();

        for(int i = 0; i < n_runs; i++) {
            for (int density : densities) {
                Map<String, String> resultMap = simpleRun(rank, ranks_cnt, n, density, rand, ring, dropType, args);
                if (rank == 0) {
                    System.out.println("=======================================================");
                    System.out.println("=======================================================");
                    System.out.println();
                    resultList.add(resultMap);
                }
            }
        }

        return resultList;
    }

    private static List<Map<String, String>> scalingTestRun(
            int n_runs, int rank, int ranks_cnt, int[] n_sizes, int density, Random rand, Ring ring, int dropType,
            String[] args
    ) throws MPIException, IOException, InterruptedException, ClassNotFoundException {
        if(rank == 0) System.out.println("Start scaling test...\n");
        List<Map<String, String>> resultList = new ArrayList<>();

        for(int i = 0; i < n_runs; i++) {
            for (int n : n_sizes) {
                Map<String, String> resultMap = simpleRun(rank, ranks_cnt, n, density, rand, ring, dropType, args);
                if (rank == 0) {
                    System.out.println("=======================================================");
                    System.out.println("=======================================================");
                    System.out.println();
                    resultList.add(resultMap);
                }
            }
        }

        return resultList;
    }

    public static void createCSV(String path, List<Map<String, String>> list) throws IOException{
        List<String> headers = list.stream().flatMap(map -> map.keySet().stream()).distinct().collect(Collectors.toList());

        try(FileWriter writer= new FileWriter(path, true);){
            for (String string : headers) {
                writer.write(string);
                writer.write(",");
            }
            writer.write("\r\n");

            for (Map<String, String> lmap : list) {
                for (Map.Entry<String, String> string2 : lmap.entrySet()) {
                    writer.write(string2.getValue());
                    writer.write(",");
                }
                writer.write("\r\n");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static double getSumOfElements(MatrixS matrix, boolean applyABS) {
        double sum = 0.0;
        for (int i = 0; i < matrix.M.length; i++) {
            for (int j = 0; j < matrix.M[i].length; j++) {
                sum += applyABS ? Math.abs(matrix.M[i][j].doubleValue()) : matrix.M[i][j].doubleValue();
            }
        }
        return sum;
    }

    private static double round(double value, int places) {
        if (places < 0) throw new IllegalArgumentException();

        long factor = (long) Math.pow(10, places);
        value = value * factor;
        long tmp = Math.round(value);
        return (double) tmp / factor;
    }
}


/*
mvn compile -DskipTests
*/

/*

$ mpirun --hostfile hostfile -np 2 java -cp $HOME/dap/target/classes:$HOME/.m2/repository/org/apache/logging/log4j/log4j-api/2.12.1/log4j-api-2.12.1.jar:$HOME/.m2/repository/org/apache/logging/log4j/log4j-core/2.12.1/log4j-core-2.12.1.jar com/mathpar/students/KAU/goryslavets/SparseWSMatrixInverse

 */