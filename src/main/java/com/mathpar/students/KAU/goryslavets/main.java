package com.mathpar.students.KAU.goryslavets;

import com.mathpar.matrix.MatrixS;
import com.mathpar.number.Element;
import com.mathpar.number.NumberR;
import com.mathpar.number.Ring;
import com.mathpar.parallel.dap.core.DispThread;
import mpi.MPI;
import mpi.MPIException;

import java.io.IOException;
import java.util.Random;

/*

Run command:

$ mpirun --hostfile hostfile java -cp $HOME/dap/target/classes:$HOME/.m2/repository/org/apache/logging/log4j/log4j-api/2.12.1/log4j-api-2.12.1.jar:$HOME/.m2/repository/org/apache/logging/log4j/log4j-core/2.12.1/log4j-core-2.12.1.jar com/mathpar/students/KAU/goryslavets/main

 */

public class main {

    public static void main(String[] args) throws MPIException, InterruptedException, ClassNotFoundException, IOException {

        MPI.Init(args);

        int rank = MPI.COMM_WORLD.getRank();

        final int n = 4;
        // final int maxRandomValue = 5;
        final int dropType = 48;
        final int density = 100;

        Ring ring = new Ring("R[]");

        Random rand = new Random(18);

        MatrixS a = new MatrixS(n, n, density, new int[] {3}, rand, NumberR.ONE, ring);


        DispThread dispT = new DispThread(1, args, ring);

        if (rank == 0) {
            System.out.println(a.print(ring)); // show input matrix
        }

        dispT.execute(dropType, 0, args, new MatrixS[]{a}, ring);

        if (rank == 0) {
            MatrixS result = (MatrixS) dispT.getResult()[0];
            System.out.println("\n");
            System.out.println(result.print(ring)); // show result matrix

            System.out.println("check = \n " + a.multiply(result, ring));
        }


        dispT.counter.DoneThread();
        dispT.counter.thread.join();
        MPI.Finalize();
    }
}
