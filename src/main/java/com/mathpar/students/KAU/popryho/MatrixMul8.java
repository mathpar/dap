package com.mathpar.students.KAU.popryho;

import com.mathpar.matrix.MatrixS;
import com.mathpar.number.NumberZ;
import com.mathpar.number.Ring;
import com.mathpar.parallel.utils.MPITransport;
import mpi.MPI;
import mpi.MPIException;

import java.io.IOException;
import java.util.Random;

/*
mpirun  --hostfile hostfile  -np 8 java  -cp ./target/classes com/mathpar/students/KAU/popryho/MatrixMul8
 */

/*
I'm processor 1
I'm processor 2
I'm processor 5
I'm processor 7
I'm processor 3
I'm processor 4
I'm processor 6
A =
[[23, 8,  17, 28, 7,  28, 0,  29]
 [19, 11, 15, 15, 27, 4,  11, 13]
 [29, 13, 19, 15, 10, 31, 1,  5 ]
 [9,  2,  22, 23, 10, 9,  11, 27]
 [13, 15, 21, 23, 19, 19, 23, 24]
 [5,  17, 14, 18, 4,  2,  25, 26]
 [17, 21, 8,  2,  29, 14, 7,  20]
 [20, 31, 10, 17, 5,  6,  27, 10]]
B =
[[20, 6,  1,  31, 4,  9,  19, 24]
 [31, 21, 29, 1,  7,  12, 17, 10]
 [2,  10, 18, 30, 1,  25, 29, 3 ]
 [17, 24, 25, 19, 6,  12, 0,  25]
 [5,  6,  14, 14, 16, 22, 9,  21]
 [23, 15, 18, 20, 29, 28, 10, 21]
 [8,  2,  8,  29, 13, 23, 26, 11]
 [26, 2,  26, 20, 6,  20, 19, 30]]
RES =
[[2651, 1668, 2617, 3001, 1431, 2582, 1960, 2988]
 [1659, 1125, 1859, 2372, 1027, 2077, 1799, 2148]
 [2177, 1534, 1959, 2656, 1418, 2283, 1844, 2280]
 [1724, 1139, 2130, 2557, 936,  2196, 1822, 2207]
 [2498, 1648, 2817, 3278, 1614, 3057, 2526, 2871]
 [1903, 1115, 2168, 2275, 864,  2054, 1990, 1963]
 [2084, 1109, 2054, 2115, 1316, 2220, 1875, 2272]
 [2309, 1473, 2178, 2447, 1074, 2105, 2194, 2073]]
 */

class MatrixMul8 {
    public static void main(String[] args)throws MPIException, IOException,ClassNotFoundException
    {
        Ring ring = new Ring("Z[]");
        MPI.Init(args);
        int rank = MPI.COMM_WORLD.getRank();
        if (rank == 0) {
            int ord = 8;
            int den = 10000;
            Random rnd = new Random();
            MatrixS A = new MatrixS(ord, ord, den, new int[] {5}, rnd, NumberZ.ONE, ring);
            System.out.println("A = " + A);
            MatrixS B = new MatrixS(ord, ord, den, new int[] {5}, rnd, NumberZ.ONE, ring);
            System.out.println("B = " + B);
            MatrixS D = null;
            MatrixS[] AA = A.split();
            MatrixS[] BB = B.split();
            int tag = 0;
            MPITransport.sendObjectArray(new Object[] {AA[1], BB[2]},0,2, 1, tag);
            MPITransport.sendObjectArray(new Object[] {AA[0], BB[1]},0,2, 2, tag);
            MPITransport.sendObjectArray(new Object[] {AA[1], BB[3]},0,2, 3, tag);
            MPITransport.sendObjectArray(new Object[] {AA[2], BB[0]},0,2, 4, tag);
            MPITransport.sendObjectArray(new Object[] {AA[3], BB[2]},0,2, 5, tag);
            MPITransport.sendObjectArray(new Object[] {AA[2], BB[1]},0,2, 6, tag);
            MPITransport.sendObjectArray(new Object[] {AA[3], BB[3]},0,2, 7, tag);
            MatrixS[] DD = new MatrixS[4];
            DD[0] = (AA[0].multiply(BB[0], ring)).add((MatrixS) MPITransport.recvObject(1, 3),ring);
            DD[1] = (MatrixS) MPITransport.recvObject(2, 3);
            DD[2] = (MatrixS) MPITransport.recvObject(4, 3);
            DD[3] = (MatrixS) MPITransport.recvObject(6, 3);
            D = MatrixS.join(DD);
            System.out.println("RES = " + D.toString());
        }
        else
        {
            System.out.println("I'm processor " + rank);
            Object[] b = new Object[2];
            MPITransport.recvObjectArray(b,0,2,0, 0);
            MatrixS[] a = new MatrixS[b.length];
            for (int i = 0; i < b.length; i++)
                a[i] = (MatrixS) b[i];
            MatrixS res = a[0].multiply(a[1], ring);
            if (rank % 2 == 0)
            {
                MatrixS p = res.add((MatrixS) MPITransport.recvObject(rank + 1, 3), ring);
                MPITransport.sendObject(p, 0, 3);
            }
            else
            {
                MPITransport.sendObject(res, rank - 1, 3);
            }
        }
        MPI.Finalize();
    }
}

