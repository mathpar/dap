package com.mathpar.students.KAU.popryho;

import com.mathpar.NAUKMA.examples.Transport;
import com.mathpar.matrix.MatrixS;
import com.mathpar.number.Element;
import com.mathpar.number.NumberZ;
import com.mathpar.number.Ring;
import com.mathpar.number.VectorS;
import mpi.MPI;
import mpi.MPIException;

import java.io.IOException;
import java.util.Random;

/*
    mpirun  --hostfile hostfile  -np 4 java  -cp ./target/classes com/mathpar/students/KAU/popryho/MultiplyMatrixToVector
 */

/*
Matrix A =
[[30, 12, 11, 21, 8,  25, 27, 2,  31, 2,  3,  8,  20, 18, 25, 0 ]
 [13, 24, 10, 30, 22, 5,  4,  14, 11, 7,  4,  15, 5,  4,  31, 14]
 [3,  26, 20, 7,  26, 22, 17, 24, 0,  18, 5,  8,  21, 19, 6,  23]
 [19, 2,  3,  6,  24, 10, 13, 9,  17, 19, 1,  10, 9,  29, 24, 1 ]
 [18, 0,  19, 18, 18, 2,  1,  7,  17, 25, 19, 14, 2,  2,  2,  20]
 [16, 1,  30, 3,  12, 27, 26, 25, 20, 17, 22, 9,  5,  31, 13, 23]
 [3,  21, 0,  15, 8,  21, 2,  6,  11, 13, 5,  20, 19, 21, 4,  16]
 [28, 11, 19, 13, 9,  14, 4,  21, 11, 25, 27, 4,  16, 2,  20, 6 ]
 [15, 19, 18, 21, 19, 7,  12, 13, 6,  7,  19, 17, 31, 5,  6,  2 ]
 [15, 26, 26, 28, 8,  28, 3,  21, 14, 3,  8,  17, 3,  30, 6,  8 ]
 [14, 16, 3,  28, 11, 15, 22, 20, 24, 31, 21, 29, 31, 10, 31, 2 ]
 [31, 4,  11, 9,  0,  29, 16, 17, 23, 7,  21, 10, 21, 10, 28, 21]
 [23, 5,  10, 28, 9,  6,  26, 19, 28, 12, 3,  16, 10, 28, 7,  8 ]
 [7,  2,  30, 12, 10, 9,  1,  2,  27, 29, 26, 6,  0,  31, 12, 3 ]
 [2,  9,  31, 14, 5,  5,  15, 3,  8,  8,  22, 3,  25, 19, 22, 13]
 [29, 11, 11, 27, 29, 24, 16, 31, 21, 28, 16, 30, 25, 21, 16, 12]]
Vector B = [9, 22, 26, 9, 22, 1, 13, 28, 21, 9, 4, 14, 3, 6, 4, 8]
 AA(1) =
[[18, 0,  19, 18, 18, 2,  1,  7,  17, 25, 19, 14, 2,  2,  2,  20]
 [16, 1,  30, 3,  12, 27, 26, 25, 20, 17, 22, 9,  5,  31, 13, 23]
 [3,  21, 0,  15, 8,  21, 2,  6,  11, 13, 5,  20, 19, 21, 4,  16]
 [28, 11, 19, 13, 9,  14, 4,  21, 11, 25, 27, 4,  16, 2,  20, 6 ]]
 BB(1) = [9, 22, 26, 9, 22, 1, 13, 28, 21, 9, 4, 14, 3, 6, 4, 8]
 VV(1) = [2465, 3526, 1990, 2765]
send result from(1)
 AA(2) =
[[15, 19, 18, 21, 19, 7,  12, 13, 6,  7,  19, 17, 31, 5,  6,  2 ]
 [15, 26, 26, 28, 8,  28, 3,  21, 14, 3,  8,  17, 3,  30, 6,  8 ]
 [14, 16, 3,  28, 11, 15, 22, 20, 24, 31, 21, 29, 31, 10, 31, 2 ]
 [31, 4,  11, 9,  0,  29, 16, 17, 23, 7,  21, 10, 21, 10, 28, 21]]
 BB(2) = [9, 22, 26, 9, 22, 1, 13, 28, 21, 9, 4, 14, 3, 6, 4, 8]
 VV(2) = [2821, 3334, 3477, 2620]
send result from(2)
 AA(3) =
[[23, 5,  10, 28, 9,  6,  26, 19, 28, 12, 3,  16, 10, 28, 7,  8 ]
 [7,  2,  30, 12, 10, 9,  1,  2,  27, 29, 26, 6,  0,  31, 12, 3 ]
 [2,  9,  31, 14, 5,  5,  15, 3,  8,  8,  22, 3,  25, 19, 22, 13]
 [29, 11, 11, 27, 29, 24, 16, 31, 21, 28, 16, 30, 25, 21, 16, 12]]
 BB(3) = [9, 22, 26, 9, 22, 1, 13, 28, 21, 9, 4, 14, 3, 6, 4, 8]
 VV(3) = [3125, 2567, 2293, 4308]
send result from(3)
RESULT: A*B=[2465, 3526, 1990, 2765, 2821, 3334, 3477, 2620, 3125, 2567, 2293, 4308, 3125, 2567, 2293, 4308]
 */

public class MultiplyMatrixToVector {
    public static void main(String[] args) throws MPIException,
            IOException, ClassNotFoundException {

        Ring ring = new Ring("Z[]");
        MPI.Init(args);
        int rank = MPI.COMM_WORLD.getRank();
        int size = MPI.COMM_WORLD.getSize();
        int ord = 16; // matrix size (ord х ord)
        int k = ord / size; // the number of rows for each processor
        int n = ord - k * (size - 1); // the last portion will be n (less than k) elements )
        if (rank == 0) {
            int den = 10000; // for density=100%
            Random rnd = new Random();
            MatrixS A = new MatrixS(ord, ord, den, new int[]{5},rnd, NumberZ.ONE, ring);
            System.out.println("Matrix A = " + A.toString());
            VectorS B = new VectorS(ord, den, new int[]{5},rnd, ring);
            System.out.println("Vector B = " + B.toString());
            Element[] result = new Element[ord]; // For resulting vector
            for (int j = 1; j < size; j++) {
                Element[][] Mj=new Element[k][];
                int[][] colj=new int[k][];
                for (int s = 0; s < k; s++) {
                    Mj[s]=A.M[s+j*k]; colj[s]=A.col[s+j*k];
                }
                MatrixS Aj=new MatrixS(Mj,colj);
                Transport.sendObject(Aj, j, 1);
                Transport.sendObject(B, j, 2);
            }
            Element[][] M0=new Element[n][];  int[][] col0=new int[n][];
            for (int s = 0; s < n; s++) {
                M0[s]=A.M[s+(size-1)*k]; col0[s]=A.col[s+(size-1)*k];
            }
            MatrixS A0=new MatrixS(M0,col0);
            VectorS V0=A0.multiplyByColumn(B.transpose(ring), ring);
            System.arraycopy(V0.V, 0, result, (size-1)*k, n);
            for (int t = 1; t < size; t++) {
                VectorS Vt = (VectorS)Transport.recvObject(t, t);
                System.arraycopy(Vt.V, 0, result,(t - 1) * k, k);
            }
            System.out.println("RESULT: A*B=" + new VectorS(result).toString(ring));
        } else {
            MatrixS AA = (MatrixS)Transport.recvObject(0, 1 );
            System.out.println(  " AA("+rank+") = " + AA.toString(ring));
            VectorS BB = (VectorS)Transport.recvObject(0, 2 );
            System.out.println(  " BB("+rank+") = " + BB.toString(ring));
            VectorS VV=AA.multiplyByColumn(BB.transpose(ring), ring);
            System.out.println(  " VV("+rank+") = " + VV.toString(ring));
            Transport.sendObject(VV, 0,  rank);
            System.out.println("send result from("+rank+")");
        }
        MPI.Finalize();
    }
}
