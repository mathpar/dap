package com.mathpar.students.KAU.popryho;
import com.mathpar.log.MpiLogger;
import com.mathpar.matrix.MatrixD;
import com.mathpar.number.Element;
import com.mathpar.number.Ring;
import com.mathpar.parallel.dap.core.DispThread;
import com.mathpar.parallel.dap.core.Drop;
import com.mathpar.parallel.dap.multiply.MatrixD.MatrDMult8;

import com.mathpar.parallel.dap.multiply.MatrixD.MatrDMultiplyAdd;
import mpi.MPI;
import mpi.MPIException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;

public class MatrixInverse extends Drop {
    private final static MpiLogger LOGGER = MpiLogger.getLogger(MatrixInverse.class);
    private static int leafSize = 4;

    private static int[][] _arcs = new int[][]{
            {1, 0, 0,    2, 1, 1,    3, 2, 0,    4, 1, 1,    4, 3, 2},      // Input #0
            {2, 0, 0,    3, 0, 1,    8, 0, 2},                              // Inv #1
            {7, 0, 0,    8, 0, 0},                                          // Mul #2
            {4, 0, 0,    6, 0, 1},                                          // Mul #3
            {5, 0, 0},                                                      // MulAdd #4
            {6, 0, 0,    7, 0, 1,    9, 0, 3},                              // Inv #5 -> output
            {8, 0, 1,    9, 0, 2},                                          // Mul #6 -> output
            {9, 0, 1},                                                      // Mul #7 -> output
            {9, 0, 0},                                                      // MulAdd #8 -> output
            {}                                                              // Output #9
    };

    public MatrixInverse() {
        inData = new Element[1];
        outData = new Element[1];
        numberOfMainComponents = 1;
        numberOfMainComponentsAtOutput = 5;
        type = 1337;
        resultForOutFunctionLength = 4;
        inputDataLength = 1;
        number = cnum++;
        arcs = _arcs;
    }

    @Override
    public ArrayList<Drop> doAmin() {
        ArrayList<Drop> amin = new ArrayList<>();
        amin.add(new MatrixInverse());
        amin.add(new MatrDMult8());
        amin.add(new MatrDMult8());
        amin.add(new MatrDMultiplyAdd());
        amin.add(new MatrixInverse());
        amin.add(new MatrDMult8());
        amin.add(new MatrDMult8());
        amin.add(new MatrDMultiplyAdd());
        return amin;
    }

    protected MatrixD inverse_sequential(MatrixD m, Ring r) {
        return (MatrixD) m.inverse(r);
    }

    @Override
    public void sequentialCalc(Ring r) {
        MatrixD m = (MatrixD)inData[0];
        outData[0] = inverse_sequential(m, r);
    }

    @Override
    public MatrixD[] inputFunction(Element[] input, int key, Ring ring) {

        MatrixD ms = (MatrixD) input[0];
        MatrixD[] blocks = ms.splitTo4();

        MatrixD[] res = new MatrixD[4];
        res[0] = blocks[0].negate(ring);
        res[1] = blocks[1];
        res[2] = blocks[2];
        res[3] = blocks[3];

        return res;
    }

    @Override
    public Element[] outputFunction(Element[] input, Ring ring) {
        MatrixD[] resInv = new MatrixD[4];

        resInv[0] = ((MatrixD) input[0]).negate(ring);
        resInv[1] = ((MatrixD) input[1]).negate(ring);
        resInv[2] = (MatrixD) input[2];
        resInv[3] = (MatrixD) input[3];

        return new MatrixD[]{MatrixD.join(resInv)};
    }

    @Override
    public boolean isItLeaf() {
        MatrixD m = (MatrixD) inData[0];
        return  m.M.length <= leafSize;
    }

    @Override
    public void setLeafSize(int dataSize) {
        leafSize = dataSize;
    }

    public static void setLeafSizeStatic(int dataSize) {
        leafSize = dataSize;
    }

    public static long[][] random_matrix(int size) {
        Random random = new Random();
        long[][] m = new long[size][size];
        for (int i = 0; i < size; ++i) {
            for (int j = 0; j < size; ++j) {
                m[i][j] = random.nextInt(50) + 1;
            }
            for (int j = i + 1; j < size; ++j) {
                m[i][j] = 0;
            }
        }
        return m;
    }

    static public void printMatrix(MatrixD m, Ring ring) {
        for (int i = 0; i < m.M.length; ++i) {
            for (int j = 0; j < m.M.length; ++j)
                System.out.printf("%3.4f ", m.getElement(i, j).doubleValue());
            System.out.println(" ");
        }
        System.out.println(" ");
    }

    static private double getSum(MatrixD m, Ring ring) {
        double sum = 0;
        for (int i = 0; i < m.M.length; ++i) {
            for (int j = 0; j < m.M.length; ++j)
                sum += Math.abs(m.getElement(i, j).doubleValue());
        }
        return sum;
    }

    static private double getIdentity(int n) {
        MatrixD m = MatrixD.zeroMatrix(n);
        return 0.0;
    }

    public static void main(String[] args) throws MPIException, InterruptedException, ClassNotFoundException, IOException, Exception {
        MPI.Init(args);
        int rank = MPI.COMM_WORLD.getRank();
        int n = 128;
        long start = 0;

        if (rank == 0) {
            for (int i = 0; i < args.length; i += 2) {
                if (args[i].equals("-size")) {
                    n = Integer.parseInt(args[i+1]);
                } else if (args[i].equals("-leaf")) {
                    setLeafSizeStatic(Integer.parseInt(args[i+1]));
                }
            }
            LOGGER.info("n = " + n + " leaf = " + leafSize);
        }

        Ring ring = new Ring("R[]");
        ring.setAccuracy(100);
        ring.setMachineEpsilonR(90);
//        ring.setFLOATPOS(120);

        MatrixD res;
        DispThread disp = new DispThread(1, args, ring);
        Element[] init = {};

        MatrixD a = null;
        if (rank == 0) {
            a = new MatrixD(random_matrix(n), ring);
            init = new MatrixD[] { a };
            if (a.M.length <= 16) {
                printMatrix(a, ring);
            }
            start = System.currentTimeMillis();
        }

        disp.execute(1337, 1, args, init, ring);

        if (rank == 0) {
            long timeElapsed = System.currentTimeMillis() - start;
            LOGGER.info("time elapsed : " + timeElapsed);
            res = (MatrixD) disp.getResult()[0];
            MatrixD shift = (MatrixD) res.multiply(a, ring).subtract(MatrixD.oneMatrixD(res.M.length, res.M.length, ring), ring);
            LOGGER.info("sum of squares of (A@A^-1 - I): " + getSum(shift, ring));
        }

        disp.counter.DoneThread();
        disp.counter.thread.join();
        MPI.Finalize();
    }

}