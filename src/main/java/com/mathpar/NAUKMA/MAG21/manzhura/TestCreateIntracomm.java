package com.mathpar.NAUKMA.MAG21.manzhura;

import mpi.Intracomm;
import mpi.MPI;
import mpi.MPIException;

import java.util.ArrayList;
import java.util.Arrays;

/*
Input:
mpirun --hostfile hostfile -np 2 java -cp /Users/amanzhura/dap/target/classes com/mathpar/NAUKMA/examples/TestCreateIntracomm

Output:

[nb02mdl029:04171] *** An error occurred in MPI_Group_incl
[nb02mdl029:04171] *** reported by process [334036993,0]
[nb02mdl029:04171] *** on communicator MPI_COMM_WORLD
[nb02mdl029:04171] *** MPI_ERR_RANK: invalid rank
[nb02mdl029:04171] *** MPI_ERRORS_ARE_FATAL (processes in this communicator will now abort,
[nb02mdl029:04171] ***    and potentially your MPI job)
[nb02mdl029:04170] 1 more process has sent help message help-mpi-errors.txt / mpi_errors_are_fatal
[nb02mdl029:04170] Set MCA parameter "orte_base_help_aggregate" to 0 to see all help / error messages

*/



public class TestCreateIntracomm {
    public static void main(String[] args) throws MPIException {
        MPI.Init(args);
        int myrank = MPI.COMM_WORLD.getRank();
        ArrayList s = new ArrayList();
        s.add(0);
        s.add(1);
        s.add(2);
        ArrayList s1 = new ArrayList();
        s1.add(3);
        s1.add(4);
        mpi.Group g = MPI.COMM_WORLD.getGroup().incl(new int[]{0, 1, 2});
        mpi.Group g2 = MPI.COMM_WORLD.getGroup().incl(new int[]{3, 4});
        Intracomm COMM_NEW = MPI.COMM_WORLD.create(g);
        Intracomm COMM_NEW_1 = MPI.COMM_WORLD.create(g2);
        int n = 5;
        int[] a = new int[n];
        if (myrank == 0 || myrank == 3) {
            for (int i = 0; i < n; i++) {
                a[i] = myrank*10+i;
            }
            System.out.println("myrank = " + myrank + ": a = " + Arrays.toString(a));
        }
        if (s.contains(myrank)) COMM_NEW.bcast(a, a.length, MPI.INT, 0);
        if (s1.contains(myrank)) COMM_NEW_1.bcast(a, a.length, MPI.INT, 0);
        if (myrank != 0 && myrank != 3) System.out.println("myrank = " + myrank + ": a = " + Arrays.toString(a));
        MPI.Finalize();
    }
}