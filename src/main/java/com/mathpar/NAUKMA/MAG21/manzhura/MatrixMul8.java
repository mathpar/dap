package com.mathpar.NAUKMA.MAG21.manzhura;

import com.mathpar.matrix.MatrixS;
import com.mathpar.number.NumberZ;
import com.mathpar.number.Ring;
import com.mathpar.parallel.utils.MPITransport;
import mpi.MPI;
import mpi.MPIException;

import java.io.IOException;
import java.util.Random;

/*
Input:
mpirun --hostfile hostfile -np 4 java -cp /Users/amanzhura/dap/target/classes com/mathpar/NAUKMA/examples/MatrixMul8

Output:

I'm processor 3
I'm processor 2
I'm processor 1
A =
[[9,  15, 3,  22, 12, 19, 18, 15]
 [0,  5,  18, 18, 25, 14, 29, 12]
 [27, 28, 15, 22, 11, 0,  14, 16]
 [22, 13, 26, 4,  31, 14, 23, 25]
 [14, 8,  25, 21, 23, 17, 15, 23]
 [9,  16, 19, 20, 24, 22, 16, 17]
 [13, 27, 20, 2,  12, 29, 24, 2 ]
 [5,  21, 14, 18, 31, 28, 10, 22]]
B =
[[1,  29, 20, 13, 3,  27, 12, 16]
 [21, 5,  30, 13, 14, 23, 9,  5 ]
 [23, 19, 10, 4,  24, 1,  14, 0 ]
 [3,  4,  24, 21, 0,  30, 15, 30]
 [25, 6,  11, 23, 7,  7,  1,  18]
 [28, 28, 12, 23, 17, 9,  31, 0 ]
 [19, 28, 9,  20, 9,  4,  7,  3 ]
 [9,  18, 16, 3,  28, 14, 29, 9 ]]
[nb02mdl029:97774] *** An error occurred in MPI_Send
[nb02mdl029:97774] *** reported by process [2119106561,0]
[nb02mdl029:97774] *** on communicator MPI_COMM_WORLD
[nb02mdl029:97774] *** MPI_ERR_RANK: invalid rank
[nb02mdl029:97774] *** MPI_ERRORS_ARE_FATAL (processes in this communicator will now abort,
[nb02mdl029:97774] ***    and potentially your MPI job)

*/


class MatrixMul8 {
    public static void main(String[] args)throws MPIException, IOException,ClassNotFoundException
    {
        Ring ring = new Ring("Z[]");
        MPI.Init(args);
        int rank = MPI.COMM_WORLD.getRank();
        if (rank == 0) {
            int ord = 8;
            int den = 10000;
            Random rnd = new Random();
            MatrixS A = new MatrixS(ord, ord, den, new int[] {5}, rnd, NumberZ.ONE, ring);
            System.out.println("A = " + A);
            MatrixS B = new MatrixS(ord, ord, den, new int[] {5}, rnd, NumberZ.ONE, ring);
            System.out.println("B = " + B);
            MatrixS D = null;
            MatrixS[] AA = A.split();
            MatrixS[] BB = B.split();
            int tag = 0;
            MPITransport.sendObjectArray(new Object[] {AA[1], BB[2]},0,2, 1, tag);
            MPITransport.sendObjectArray(new Object[] {AA[0], BB[1]},0,2, 2, tag);
            MPITransport.sendObjectArray(new Object[] {AA[1], BB[3]},0,2, 3, tag);
            MPITransport.sendObjectArray(new Object[] {AA[2], BB[0]},0,2, 4, tag);
            MPITransport.sendObjectArray(new Object[] {AA[3], BB[2]},0,2, 5, tag);
            MPITransport.sendObjectArray(new Object[] {AA[2], BB[1]},0,2, 6, tag);
            MPITransport.sendObjectArray(new Object[] {AA[3], BB[3]},0,2, 7, tag);
            MatrixS[] DD = new MatrixS[4];
            DD[0] = (AA[0].multiply(BB[0], ring)).add((MatrixS) MPITransport.recvObject(1, 3),ring);
            DD[1] = (MatrixS) MPITransport.recvObject(2, 3);
            DD[2] = (MatrixS) MPITransport.recvObject(4, 3);
            DD[3] = (MatrixS) MPITransport.recvObject(6, 3);
            D = MatrixS.join(DD);
            System.out.println("RES = " + D.toString());
        }
        else
        {
            System.out.println("I'm processor " + rank);
            Object[] b = new Object[2];
            MPITransport.recvObjectArray(b,0,2,0, 0);
            MatrixS[] a = new MatrixS[b.length];
            for (int i = 0; i < b.length; i++)
                a[i] = (MatrixS) b[i];
            MatrixS res = a[0].multiply(a[1], ring);
            if (rank % 2 == 0)
            {
                MatrixS p = res.add((MatrixS) MPITransport.recvObject(rank + 1, 3), ring);
                MPITransport.sendObject(p, 0, 3);
            }
            else
            {
                MPITransport.sendObject(res, rank - 1, 3);
            }
        }
        MPI.Finalize();
    }
}

