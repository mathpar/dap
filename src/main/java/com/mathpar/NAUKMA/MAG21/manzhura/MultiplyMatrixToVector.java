package com.mathpar.NAUKMA.MAG21.manzhura;

import com.mathpar.matrix.MatrixS;
import com.mathpar.number.Element;
import com.mathpar.number.NumberZ;
import com.mathpar.number.Ring;
import com.mathpar.number.VectorS;
import mpi.MPI;
import mpi.MPIException;

import java.io.IOException;
import java.util.Random;

/*
Input:
mpirun --hostfile hostfile -np 5 java -cp /Users/amanzhura/dap/target/classes com/mathpar/NAUKMA/examples/MultiplyMatrixToVector

Output:

Matrix A =
[[2,  1,  10, 1,  6,  29, 19, 22, 11, 0,  14, 1,  27, 24, 19, 30]
 [22, 4,  9,  15, 26, 16, 26, 28, 18, 20, 23, 8,  2,  10, 7,  21]
 [17, 16, 21, 26, 24, 30, 26, 2,  10, 9,  8,  7,  4,  24, 25, 15]
 [23, 5,  24, 7,  19, 23, 14, 30, 22, 30, 15, 4,  22, 16, 5,  4 ]
 [10, 2,  29, 30, 31, 9,  17, 9,  25, 19, 21, 7,  8,  0,  6,  10]
 [14, 6,  16, 7,  14, 4,  30, 24, 22, 5,  7,  23, 16, 25, 22, 24]
 [5,  15, 26, 7,  18, 22, 16, 3,  16, 21, 14, 26, 31, 8,  17, 6 ]
 [18, 29, 8,  7,  0,  14, 2,  1,  10, 1,  21, 24, 8,  29, 17, 10]
 [25, 15, 4,  12, 10, 28, 15, 9,  6,  8,  9,  23, 31, 8,  17, 27]
 [28, 3,  26, 31, 4,  15, 18, 17, 9,  9,  19, 31, 23, 22, 13, 17]
 [31, 25, 20, 6,  31, 21, 6,  18, 20, 4,  31, 23, 3,  23, 0,  2 ]
 [4,  16, 29, 8,  16, 29, 6,  1,  12, 23, 24, 5,  21, 26, 4,  5 ]
 [14, 6,  0,  12, 8,  23, 13, 22, 18, 1,  8,  16, 1,  14, 21, 18]
 [1,  28, 8,  25, 28, 17, 22, 8,  10, 1,  28, 0,  25, 3,  13, 30]
 [31, 15, 1,  22, 30, 27, 28, 14, 23, 22, 29, 28, 18, 6,  8,  3 ]
 [11, 30, 21, 10, 1,  3,  26, 30, 27, 0,  22, 11, 28, 7,  0,  21]]
Vector B = [9, 9, 16, 28, 24, 24, 11, 17, 27, 29, 20, 18, 13, 20, 17, 13]
 AA(1) =
[[23, 5, 24, 7,  19, 23, 14, 30, 22, 30, 15, 4,  22, 16, 5,  4 ]
 [10, 2, 29, 30, 31, 9,  17, 9,  25, 19, 21, 7,  8,  0,  6,  10]
 [14, 6, 16, 7,  14, 4,  30, 24, 22, 5,  7,  23, 16, 25, 22, 24]]
 BB(1) = [9, 9, 16, 28, 24, 24, 11, 17, 27, 29, 20, 18, 13, 20, 17, 13]
 VV(1) = [5083, 4820, 4489]
send result from(1)
 AA(2) =
[[5,  15, 26, 7,  18, 22, 16, 3, 16, 21, 14, 26, 31, 8,  17, 6 ]
 [18, 29, 8,  7,  0,  14, 2,  1, 10, 1,  21, 24, 8,  29, 17, 10]
 [25, 15, 4,  12, 10, 28, 15, 9, 6,  8,  9,  23, 31, 8,  17, 27]]
 BB(2) = [9, 9, 16, 28, 24, 24, 11, 17, 27, 29, 20, 18, 13, 20, 17, 13]
 VV(2) = [4698, 3376, 4181]
send result from(2)
 AA(3) =
[[28, 3,  26, 31, 4,  15, 18, 17, 9,  9,  19, 31, 23, 22, 13, 17]
 [31, 25, 20, 6,  31, 21, 6,  18, 20, 4,  31, 23, 3,  23, 0,  2 ]
 [4,  16, 29, 8,  16, 29, 6,  1,  12, 23, 24, 5,  21, 26, 4,  5 ]]
 BB(3) = [9, 9, 16, 28, 24, 24, 11, 17, 27, 29, 20, 18, 13, 20, 17, 13]
 VV(3) = [5129, 4827, 4518]
send result from(3)
 AA(4) =
[[14, 6,  0, 12, 8,  23, 13, 22, 18, 1,  8,  16, 1,  14, 21, 18]
 [1,  28, 8, 25, 28, 17, 22, 8,  10, 1,  28, 0,  25, 3,  13, 30]
 [31, 15, 1, 22, 30, 27, 28, 14, 23, 22, 29, 28, 18, 6,  8,  3 ]]
 BB(4) = [9, 9, 16, 28, 24, 24, 11, 17, 27, 29, 20, 18, 13, 20, 17, 13]
 VV(4) = [3624, 4402, 5832]
send result from(4)
RESULT: A*B=[5083, 4820, 4489, 4698, 3376, 4181, 5129, 4827, 4518, 3624, 4402, 5832, 3624, 4402, 5832, 4021]

*/




public class MultiplyMatrixToVector {
    public static void main(String[] args) throws MPIException,
            IOException, ClassNotFoundException {

        Ring ring = new Ring("Z[]");
        MPI.Init(args);
        int rank = MPI.COMM_WORLD.getRank();
        int size = MPI.COMM_WORLD.getSize();
        int ord = 16; // matrix size (ord х ord)
        int k = ord / size; // the number of rows for each processor
        int n = ord - k * (size - 1); // the last portion will be n (less than k) elements )
        if (rank == 0) {
            int den = 10000; // for density=100%
            Random rnd = new Random();
            MatrixS A = new MatrixS(ord, ord, den, new int[]{5},rnd, NumberZ.ONE, ring);
            System.out.println("Matrix A = " + A.toString());
            VectorS B = new VectorS(ord, den, new int[]{5},rnd, ring);
            System.out.println("Vector B = " + B.toString());
            Element[] result = new Element[ord]; // For resulting vector
            for (int j = 1; j < size; j++) {
                Element[][] Mj=new Element[k][];
                int[][] colj=new int[k][];
                for (int s = 0; s < k; s++) {
                    Mj[s]=A.M[s+j*k]; colj[s]=A.col[s+j*k];
                }
                MatrixS Aj=new MatrixS(Mj,colj);
                Transport.sendObject(Aj, j, 1);
                Transport.sendObject(B, j, 2);
            }
            Element[][] M0=new Element[n][];  int[][] col0=new int[n][];
            for (int s = 0; s < n; s++) {
                M0[s]=A.M[s+(size-1)*k]; col0[s]=A.col[s+(size-1)*k];
            }
            MatrixS A0=new MatrixS(M0,col0);
            VectorS V0=A0.multiplyByColumn(B.transpose(ring), ring);
            System.arraycopy(V0.V, 0, result, (size-1)*k, n);
            for (int t = 1; t < size; t++) {
                VectorS Vt = (VectorS) Transport.recvObject(t, t);
                System.arraycopy(Vt.V, 0, result,(t - 1) * k, k);
            }
            System.out.println("RESULT: A*B=" + new VectorS(result).toString(ring));
        } else {
            MatrixS AA = (MatrixS) Transport.recvObject(0, 1 );
            System.out.println(  " AA("+rank+") = " + AA.toString(ring));
            VectorS BB = (VectorS) Transport.recvObject(0, 2 );
            System.out.println(  " BB("+rank+") = " + BB.toString(ring));
            VectorS VV=AA.multiplyByColumn(BB.transpose(ring), ring);
            System.out.println(  " VV("+rank+") = " + VV.toString(ring));
            Transport.sendObject(VV, 0,  rank);
            System.out.println("send result from("+rank+")");
        }
        MPI.Finalize();
    }
}
