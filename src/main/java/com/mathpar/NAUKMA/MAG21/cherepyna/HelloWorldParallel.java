package com.mathpar.NAUKMA.MAG21.cherepyna;

import mpi.MPI;
import mpi.MPIException;

public class HelloWorldParallel {
    public static void main(String[] args) throws MPIException {
        //iнiцiалiзацiя паралельної частини
        MPI.Init(args);
        //визначення номера процесора
        int myrank = MPI.COMM_WORLD.getRank();
        System.out.println("Proc num " + myrank + " Hello World");
        //завершення паралельної частини
        MPI.Finalize();
    }
}

/*
mpirun --hostfile /home/yelyzaveta/hostfile -np 7 java -cp /home/yelyzaveta/dap/target/classes com/mathpar/NAUKMA/MAG21/cherepyna/HelloWorldParallel
Proc num 0 Hello WorldProc num 3 Hello World
Proc num 5 Hello WorldProc num 4 Hello World


Proc num 1 Hello World
Proc num 2 Hello World
Proc num 6 Hello World
*/
