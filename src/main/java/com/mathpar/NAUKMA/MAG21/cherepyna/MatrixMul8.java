package com.mathpar.NAUKMA.MAG21.cherepyna;

import com.mathpar.matrix.MatrixS;
import com.mathpar.number.NumberZ;
import com.mathpar.number.Ring;
import com.mathpar.parallel.utils.MPITransport;
import mpi.MPI;
import mpi.MPIException;

import java.io.IOException;
import java.util.Random;

class MatrixMul8 {
    public static void main(String[] args)throws MPIException, IOException,ClassNotFoundException
    {
        Ring ring = new Ring("Z[]");
        MPI.Init(args);
        int rank = MPI.COMM_WORLD.getRank();
        if (rank == 0) {
            int ord = 8;
            int den = 10000;
            Random rnd = new Random();
            MatrixS A = new MatrixS(ord, ord, den, new int[] {5}, rnd, NumberZ.ONE, ring);
            System.out.println("A = " + A);
            MatrixS B = new MatrixS(ord, ord, den, new int[] {5}, rnd, NumberZ.ONE, ring);
            System.out.println("B = " + B);
            MatrixS D = null;
            MatrixS[] AA = A.split();
            MatrixS[] BB = B.split();
            int tag = 0;
            MPITransport.sendObjectArray(new Object[] {AA[1], BB[2]},0,2, 1, tag);
            MPITransport.sendObjectArray(new Object[] {AA[0], BB[1]},0,2, 2, tag);
            MPITransport.sendObjectArray(new Object[] {AA[1], BB[3]},0,2, 3, tag);
            MPITransport.sendObjectArray(new Object[] {AA[2], BB[0]},0,2, 4, tag);
            MPITransport.sendObjectArray(new Object[] {AA[3], BB[2]},0,2, 5, tag);
            MPITransport.sendObjectArray(new Object[] {AA[2], BB[1]},0,2, 6, tag);
            MPITransport.sendObjectArray(new Object[] {AA[3], BB[3]},0,2, 7, tag);
            MatrixS[] DD = new MatrixS[4];
            DD[0] = (AA[0].multiply(BB[0], ring)).add((MatrixS) MPITransport.recvObject(1, 3),ring);
            DD[1] = (MatrixS) MPITransport.recvObject(2, 3);
            DD[2] = (MatrixS) MPITransport.recvObject(4, 3);
            DD[3] = (MatrixS) MPITransport.recvObject(6, 3);
            D = MatrixS.join(DD);
            System.out.println("RES = " + D.toString());
        }
        else
        {
            System.out.println("I'm processor " + rank);
            Object[] b = new Object[2];
            MPITransport.recvObjectArray(b,0,2,0, 0);
            MatrixS[] a = new MatrixS[b.length];
            for (int i = 0; i < b.length; i++)
                a[i] = (MatrixS) b[i];
            MatrixS res = a[0].multiply(a[1], ring);
            if (rank % 2 == 0)
            {
                MatrixS p = res.add((MatrixS) MPITransport.recvObject(rank + 1, 3), ring);
                MPITransport.sendObject(p, 0, 3);
            }
            else
            {
                MPITransport.sendObject(res, rank - 1, 3);
            }
        }
        MPI.Finalize();
    }
}

/*
mpirun --hostfile /home/yelyzaveta/hostfile -np 7 java -cp /home/yelyzaveta/dap/target/classes com/mathpar/NAUKMA/MAG21/cherepyna/MatrixMul8
Invalid MIT-MAGIC-COOKIE-1 keyInvalid MIT-MAGIC-COOKIE-1 keyInvalid MIT-MAGIC-COOKIE-1 keyInvalid MIT-MAGIC-COOKIE-1 keyInvalid MIT-MAGIC-COOKIE-1 keyInvalid MIT-MAGIC-COOKIE-1 keyInvalid MIT-MAGIC-COOKIE-1 keyInvalid MIT-MAGIC-COOKIE-1 keyI'm processor 4I'm processor 5

I'm processor 2I'm processor 1

I'm processor 6
I'm processor 3
A = 
[[3,  21, 25, 6,  6,  10, 26, 24]
 [11, 22, 19, 24, 9,  11, 15, 10]
 [5,  2,  28, 0,  6,  23, 19, 25]
 [27, 5,  5,  19, 10, 3,  8,  20]
 [29, 29, 3,  25, 8,  22, 22, 28]
 [30, 30, 13, 12, 21, 15, 6,  8 ]
 [22, 14, 1,  30, 9,  30, 8,  2 ]
 [0,  7,  30, 17, 3,  8,  12, 7 ]]
B = 
[[28, 26, 9,  30, 3,  6,  10, 9 ]
 [12, 8,  10, 16, 16, 5,  28, 14]
 [25, 9,  14, 9,  17, 5,  0,  16]
 [13, 7,  6,  26, 22, 27, 25, 29]
 [24, 2,  4,  13, 14, 17, 13, 22]
 [13, 12, 2,  28, 16, 3,  6,  18]
 [4,  13, 29, 25, 21, 3,  20, 27]
 [24, 30, 20, 7,  26, 20, 7,  14]]
RES =
[[4100, 3274, 1931, 2321, 3672, 2853, 3961, 3350]
 [3627, 2698, 1255, 2105, 3196, 2116, 3434, 3053]
 [2377, 1886, 837,  1943, 1905, 1443, 2316, 1454]
 [1932, 1161, 909,  863,  1298, 1021, 1344, 1178]
 [2177, 1507, 916,  1002, 1796, 1312, 1949, 1592]
 [2218, 1879, 1050, 1208, 2133, 1794, 2406, 1956]
 [2916, 2140, 1173, 1868, 2461, 1720, 2822, 2343]
 [3257, 2440, 1712, 1828, 2810, 2194, 3224, 2453]]
*/
