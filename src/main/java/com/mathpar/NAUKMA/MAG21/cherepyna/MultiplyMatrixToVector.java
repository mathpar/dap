package com.mathpar.NAUKMA.MAG21.cherepyna;

import com.mathpar.matrix.MatrixS;
import com.mathpar.number.Element;
import com.mathpar.number.NumberZ;
import com.mathpar.number.Ring;
import com.mathpar.number.VectorS;
import mpi.MPI;
import mpi.MPIException;

import java.io.IOException;
import java.util.Random;

public class MultiplyMatrixToVector {
    public static void main(String[] args) throws MPIException,
            IOException, ClassNotFoundException {

        Ring ring = new Ring("Z[]");
        MPI.Init(args);
        int rank = MPI.COMM_WORLD.getRank();
        int size = MPI.COMM_WORLD.getSize();
        int ord = 16; // matrix size (ord х ord)
        int k = ord / size; // the number of rows for each processor
        int n = ord - k * (size - 1); // the last portion will be n (less than k) elements )
        if (rank == 0) {
            int den = 10000; // for density=100%
            Random rnd = new Random();
            MatrixS A = new MatrixS(ord, ord, den, new int[]{5},rnd, NumberZ.ONE, ring);
            System.out.println("Matrix A = " + A.toString());
            VectorS B = new VectorS(ord, den, new int[]{5},rnd, ring);
            System.out.println("Vector B = " + B.toString());
            Element[] result = new Element[ord]; // For resulting vector
            for (int j = 1; j < size; j++) {
                Element[][] Mj=new Element[k][];
                int[][] colj=new int[k][];
                for (int s = 0; s < k; s++) {
                    Mj[s]=A.M[s+j*k]; colj[s]=A.col[s+j*k];
                }
                MatrixS Aj=new MatrixS(Mj,colj);
                Transport.sendObject(Aj, j, 1);
                Transport.sendObject(B, j, 2);
            }
            Element[][] M0=new Element[n][];  int[][] col0=new int[n][];
            for (int s = 0; s < n; s++) {
                M0[s]=A.M[s+(size-1)*k]; col0[s]=A.col[s+(size-1)*k];
            }
            MatrixS A0=new MatrixS(M0,col0);
            VectorS V0=A0.multiplyByColumn(B.transpose(ring), ring);
            System.arraycopy(V0.V, 0, result, (size-1)*k, n);
            for (int t = 1; t < size; t++) {
                VectorS Vt = (VectorS) Transport.recvObject(t, t);
                System.arraycopy(Vt.V, 0, result,(t - 1) * k, k);
            }
            System.out.println("RESULT: A*B=" + new VectorS(result).toString(ring));
        } else {
            MatrixS AA = (MatrixS) Transport.recvObject(0, 1 );
            System.out.println(  " AA("+rank+") = " + AA.toString(ring));
            VectorS BB = (VectorS) Transport.recvObject(0, 2 );
            System.out.println(  " BB("+rank+") = " + BB.toString(ring));
            VectorS VV=AA.multiplyByColumn(BB.transpose(ring), ring);
            System.out.println(  " VV("+rank+") = " + VV.toString(ring));
            Transport.sendObject(VV, 0,  rank);
            System.out.println("send result from("+rank+")");
        }
        

        MPI.Finalize();
    }
}

/*mpirun --hostfile /home/yelyzaveta/hostfile -np 7 java -cp /home/yelyzaveta/dap/target/classes com/mathpar/NAUKMA/MAG21/cherepyna/MultiplyMatrixToVector
Invalid MIT-MAGIC-COOKIE-1 keyInvalid MIT-MAGIC-COOKIE-1 keyInvalid MIT-MAGIC-COOKIE-1 keyInvalid MIT-MAGIC-COOKIE-1 keyInvalid MIT-MAGIC-COOKIE-1 keyInvalid MIT-MAGIC-COOKIE-1 keyInvalid MIT-MAGIC-COOKIE-1 keyInvalid MIT-MAGIC-COOKIE-1 keyMatrix A = 
[[6,  30, 16, 27, 28, 13, 13, 31, 2,  20, 19, 30, 24, 15, 30, 17]
 [20, 12, 30, 26, 10, 6,  31, 29, 27, 0,  27, 27, 2,  5,  30, 21]
 [27, 30, 26, 0,  20, 20, 12, 4,  24, 8,  14, 27, 25, 15, 31, 7 ]
 [30, 18, 15, 19, 13, 6,  15, 13, 26, 9,  24, 3,  17, 29, 27, 28]
 [0,  9,  9,  25, 9,  12, 3,  13, 6,  3,  4,  26, 28, 31, 14, 13]
 [5,  31, 18, 27, 25, 21, 3,  29, 29, 22, 29, 8,  15, 2,  18, 16]
 [16, 22, 21, 31, 18, 14, 10, 18, 3,  0,  14, 20, 26, 22, 2,  29]
 [2,  11, 30, 12, 5,  9,  19, 3,  27, 0,  1,  4,  19, 10, 27, 10]
 [0,  30, 23, 9,  7,  18, 25, 9,  8,  22, 2,  0,  11, 13, 21, 14]
 [13, 14, 11, 30, 10, 26, 24, 26, 13, 31, 28, 14, 22, 14, 14, 1 ]
 [11, 25, 5,  16, 2,  16, 9,  12, 13, 28, 25, 22, 6,  28, 30, 25]
 [29, 31, 17, 7,  0,  2,  8,  23, 10, 11, 6,  30, 15, 6,  3,  13]
 [7,  31, 24, 11, 24, 0,  16, 18, 9,  11, 12, 15, 4,  27, 21, 21]
 [11, 23, 8,  1,  30, 23, 28, 20, 11, 28, 23, 13, 23, 10, 18, 6 ]
 [12, 7,  9,  18, 4,  25, 7,  20, 20, 27, 17, 1,  28, 1,  18, 26]
 [14, 14, 8,  3,  29, 19, 13, 9,  7,  9,  7,  18, 6,  17, 19, 5 ]]
Vector B = [19, 11, 15, 29, 28, 20, 7, 9, 8, 4, 2, 7, 7, 8, 14, 0]
 AA(1) = 
[[27, 30, 26, 0,  20, 20, 12, 4,  24, 8, 14, 27, 25, 15, 31, 7 ]
 [30, 18, 15, 19, 13, 6,  15, 13, 26, 9, 24, 3,  17, 29, 27, 28]]
 BB(1) = [19, 11, 15, 29, 28, 20, 7, 9, 8, 4, 2, 7, 7, 8, 14, 0]
 VV(1) = [3483, 3292]
send result from(1)
 AA(2) = 
[[0, 9,  9,  25, 9,  12, 3, 13, 6,  3,  4,  26, 28, 31, 14, 13]
 [5, 31, 18, 27, 25, 21, 3, 29, 29, 22, 29, 8,  15, 2,  18, 16]]
 BB(2) = [19, 11, 15, 29, 28, 20, 7, 9, 8, 4, 2, 7, 7, 8, 14, 0]
 VV(2) = [2479, 3698]
send result from(2)
 AA(3) = 
[[16, 22, 21, 31, 18, 14, 10, 18, 3,  0, 14, 20, 26, 22, 2,  29]
 [2,  11, 30, 12, 5,  9,  19, 3,  27, 0, 1,  4,  19, 10, 27, 10]]
 BB(3) = [19, 11, 15, 29, 28, 20, 7, 9, 8, 4, 2, 7, 7, 8, 14, 0]
 VV(3) = [3354, 2274]
send result from(3)
 AA(4) = 
[[0,  30, 23, 9,  7,  18, 25, 9,  8,  22, 2,  0,  11, 13, 21, 14]
 [13, 14, 11, 30, 10, 26, 24, 26, 13, 31, 28, 14, 22, 14, 14, 1 ]]
 BB(4) = [19, 11, 15, 29, 28, 20, 7, 9, 8, 4, 2, 7, 7, 8, 14, 0]
 VV(4) = [2379, 3482]
send result from(4)
 AA(5) = 
[[11, 25, 5,  16, 2, 16, 9, 12, 13, 28, 25, 22, 6,  28, 30, 25]
 [29, 31, 17, 7,  0, 2,  8, 23, 10, 11, 6,  30, 15, 6,  3,  13]]
 BB(5) = [19, 11, 15, 29, 28, 20, 7, 9, 8, 4, 2, 7, 7, 8, 14, 0]
 VV(5) = [2676, 2194]
send result from(5)
 AA(6) = 
[[7,  31, 24, 11, 24, 0,  16, 18, 9,  11, 12, 15, 4,  27, 21, 21]
 [11, 23, 8,  1,  30, 23, 28, 20, 11, 28, 23, 13, 23, 10, 18, 6 ]]
 BB(6) = [19, 11, 15, 29, 28, 20, 7, 9, 8, 4, 2, 7, 7, 8, 14, 0]
 VV(6) = [2882, 3117]
send result from(6)RESULT: A*B=[3483, 3292, 2479, 3698, 3354, 2274, 2379, 3482, 2676, 2194, 2882, 3117, 2882, 3117, 2568, 2667]

*/
