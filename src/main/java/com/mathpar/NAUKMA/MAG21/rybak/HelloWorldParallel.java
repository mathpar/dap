package com.mathpar.NAUKMA.MAG21.rybak;

import mpi.MPI;
import mpi.MPIException;

/*
Input command:

mpirun --allow-run-as-root  --hostfile hostfile -np 7 java -cp DAP/target/classes/com dap/src/main/java/com/mathpar/NAUKMA/MAG21/rybak/HelloWorldParallel.java

Output result:

Proc num 6 Hello World
Proc num 5 Hello World
Proc num 3 Hello WorldProc num 1 Hello World

Proc num 2 Hello World
Proc num 0 Hello World
Proc num 4 Hello World
 */
public class HelloWorldParallel {
    public static void main(String[] args) throws MPIException {
        //iнiцiалiзацiя паралельної частини
        MPI.Init(args);
        //визначення номера процесора
        int myrank = MPI.COMM_WORLD.getRank();
        System.out.println("Proc num " + myrank + " Hello World");
        //завершення паралельної частини
        MPI.Finalize();
    }
}

