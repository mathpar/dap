package com.mathpar.NAUKMA.MAG21.baranov;

import mpi.MPI;
import mpi.MPIException;

/*
Input:
mpirun --hostfile /home/den/hostfile -np 7 java -cp /home/den/dap/target/classes com/mathpar/NAUKMA/MAG21/baranov/HelloWorldParallel
Output:
Proc num 2 Hello WorldProc num 4 Hello WorldProc num 3 Hello WorldProc num 0 Hello World



Proc num 1 Hello World
Proc num 5 Hello World
Proc num 6 Hello World


*/


public class HelloWorldParallel {
    public static void main(String[] args) throws MPIException {
        //iнiцiалiзацiя паралельної частини
        MPI.Init(args);
        //визначення номера процесора
        int myrank = MPI.COMM_WORLD.getRank();
        System.out.println("Proc num " + myrank + " Hello World");
        //завершення паралельної частини
        MPI.Finalize();
    }
}

