package com.mathpar.NAUKMA.MAG21.baranov;

import mpi.MPI;
import mpi.MPIException;

import java.util.Random;

/*
mpirun --hostfile /home/den/hostfile -np 4 java -cp /home/den/dap/target/classes com/mathpar/NAUKMA/MAG21/baranov/TestSendAndRecv
a[0]= 0.3206453298857509
a[1]= 0.24519160035831966
a[2]= 0.7885701504317207
a[3]= 0.4391998157515067
a[4]= 0.3547335003002031
Proc num 0 масив вiдправлено

a[0]= 0.3206453298857509
a[1]= 0.24519160035831966a[0]= 0.3206453298857509
a[2]= 0.7885701504317207
a[0]= 0.3206453298857509
a[3]= 0.4391998157515067a[1]= 0.24519160035831966

a[2]= 0.7885701504317207
a[4]= 0.3547335003002031a[3]= 0.4391998157515067


a[1]= 0.24519160035831966a[4]= 0.3547335003002031

a[2]= 0.7885701504317207
a[3]= 0.4391998157515067
Proc num 1 масив прийнято
Proc num 2 масив прийнято
a[4]= 0.3547335003002031




*/


public class TestSendAndRecv {
    public static void main(String[] args) throws MPIException {
        //iнiцiалiзацiя MPI
        MPI.Init(args);
        //визначення номера процесора
        int myrank = MPI.COMM_WORLD.getRank();
        //визначення кiлькостi процесорiв у групi
        int np = MPI.COMM_WORLD.getSize();
        //вхiдний параметр - розмiр масиву
        int n = 5;
        double[] a = new double[n];
        //синхронiзацiя процесорiв
        // MPI.COMM_WORLD.barrier();
        // якщо процесор з номером 0
        if (myrank == 0) {
            for (int i = 0; i < n; i++) {
                a[i] = (new Random()).nextDouble();
                System.out.println("a[" + i + "]= " + a[i]);
            }
            //передання 0-процесором елементiв усiм iншим процесорам у групi
            for (int i = 1; i < np; i++) {
                MPI.COMM_WORLD.send(a, n, MPI.DOUBLE, i, 3000);
            }
            System.out.println("Proc num " + myrank + " масив вiдправлено" + "\n");
        } else {
            //приймання i-м процесором повiдомлення вiд
            // процесора з номером 0 та тегом 3000.
            MPI.COMM_WORLD.recv(a, n, MPI.DOUBLE, 0, 3000);
            for (int i = 0; i < n; i++) {
                System.out.println("a[" + i + "]= " + a[i]);
            }
            System.out.println("Proc num " + myrank + " масив прийнято" + "\n");
        }

        // завершення паралельної частини
        MPI.Finalize();
    }
}
