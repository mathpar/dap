package com.mathpar.NAUKMA.MAG21.baranov;

import com.mathpar.matrix.MatrixS;
import com.mathpar.number.NumberZ;
import com.mathpar.number.Ring;
import com.mathpar.parallel.utils.MPITransport;
import mpi.MPI;
import mpi.MPIException;

import java.io.IOException;
import java.util.Random;

/*
mpirun --hostfile /home/den/hostfile -np 7 java -cp /home/den/dap/target/classes com/mathpar/NAUKMA/MAG21/baranov/MatrixMul8
I'm processor 3I'm processor 2I'm processor 6I'm processor 5I'm processor 1




I'm processor 4
A =
[[22, 19, 3,  23, 30, 16, 24, 9 ]
 [14, 24, 1,  25, 13, 13, 6,  5 ]
 [7,  6,  30, 6,  19, 15, 29, 15]
 [4,  0,  1,  8,  21, 1,  25, 11]
 [14, 29, 22, 31, 28, 4,  22, 21]
 [16, 7,  15, 0,  22, 14, 25, 25]
 [25, 29, 31, 19, 20, 8,  6,  26]
 [27, 19, 30, 18, 11, 13, 9,  14]]
B =
[[16, 14, 10, 14, 22, 16, 16, 30]
 [18, 11, 3,  11, 16, 21, 13, 8 ]
 [0,  24, 12, 1,  29, 20, 23, 30]
 [30, 23, 15, 28, 0,  27, 8,  7 ]
 [19, 0,  23, 27, 3,  11, 30, 27]
 [10, 30, 21, 18, 21, 24, 27, 10]
 [20, 15, 30, 10, 5,  12, 7,  20]
 [3,  6,  10, 29, 11, 2,  12, 15]]
[den-VirtualBox:03170] *** An error occurred in MPI_Send
[den-VirtualBox:03170] *** reported by process [3112828929,0]
[den-VirtualBox:03170] *** on communicator MPI_COMM_WORLD
[den-VirtualBox:03170] *** MPI_ERR_RANK: invalid rank
[den-VirtualBox:03170] *** MPI_ERRORS_ARE_FATAL (processes in this communicator will now abort,
[den-VirtualBox:03170] ***    and potentially your MPI job)

*/


class MatrixMul8 {
    public static void main(String[] args)throws MPIException, IOException,ClassNotFoundException
    {
        Ring ring = new Ring("Z[]");
        MPI.Init(args);
        int rank = MPI.COMM_WORLD.getRank();
        if (rank == 0) {
            int ord = 8;
            int den = 10000;
            Random rnd = new Random();
            MatrixS A = new MatrixS(ord, ord, den, new int[] {5}, rnd, NumberZ.ONE, ring);
            System.out.println("A = " + A);
            MatrixS B = new MatrixS(ord, ord, den, new int[] {5}, rnd, NumberZ.ONE, ring);
            System.out.println("B = " + B);
            MatrixS D = null;
            MatrixS[] AA = A.split();
            MatrixS[] BB = B.split();
            int tag = 0;
            MPITransport.sendObjectArray(new Object[] {AA[1], BB[2]},0,2, 1, tag);
            MPITransport.sendObjectArray(new Object[] {AA[0], BB[1]},0,2, 2, tag);
            MPITransport.sendObjectArray(new Object[] {AA[1], BB[3]},0,2, 3, tag);
            MPITransport.sendObjectArray(new Object[] {AA[2], BB[0]},0,2, 4, tag);
            MPITransport.sendObjectArray(new Object[] {AA[3], BB[2]},0,2, 5, tag);
            MPITransport.sendObjectArray(new Object[] {AA[2], BB[1]},0,2, 6, tag);
            MPITransport.sendObjectArray(new Object[] {AA[3], BB[3]},0,2, 7, tag);
            MatrixS[] DD = new MatrixS[4];
            DD[0] = (AA[0].multiply(BB[0], ring)).add((MatrixS) MPITransport.recvObject(1, 3),ring);
            DD[1] = (MatrixS) MPITransport.recvObject(2, 3);
            DD[2] = (MatrixS) MPITransport.recvObject(4, 3);
            DD[3] = (MatrixS) MPITransport.recvObject(6, 3);
            D = MatrixS.join(DD);
            System.out.println("RES = " + D.toString());
        }
        else
        {
            System.out.println("I'm processor " + rank);
            Object[] b = new Object[2];
            MPITransport.recvObjectArray(b,0,2,0, 0);
            MatrixS[] a = new MatrixS[b.length];
            for (int i = 0; i < b.length; i++)
                a[i] = (MatrixS) b[i];
            MatrixS res = a[0].multiply(a[1], ring);
            if (rank % 2 == 0)
            {
                MatrixS p = res.add((MatrixS) MPITransport.recvObject(rank + 1, 3), ring);
                MPITransport.sendObject(p, 0, 3);
            }
            else
            {
                MPITransport.sendObject(res, rank - 1, 3);
            }
        }
        MPI.Finalize();
    }
}

