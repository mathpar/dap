package com.mathpar.NAUKMA.MAG21.baranov;

import com.mathpar.matrix.MatrixS;
import com.mathpar.number.Element;
import com.mathpar.number.NumberZ;
import com.mathpar.number.Ring;
import com.mathpar.number.VectorS;
import mpi.MPI;
import mpi.MPIException;

import java.io.IOException;
import java.util.Random;

/*
mpirun --hostfile /home/den/hostfile -np 7 java -cp /home/den/dap/target/classes com/mathpar/NAUKMA/MAG21/baranov/MultiplyMatrixToVector
Matrix A =
[[11, 23, 26, 21, 27, 31, 0,  18, 2,  11, 14, 9,  12, 9,  30, 4 ]
 [13, 24, 25, 29, 4,  25, 22, 15, 14, 26, 21, 28, 13, 1,  27, 0 ]
 [26, 22, 16, 23, 7,  19, 22, 10, 25, 16, 11, 31, 4,  10, 2,  2 ]
 [8,  20, 29, 14, 11, 1,  14, 0,  24, 16, 15, 29, 23, 24, 0,  2 ]
 [14, 22, 0,  13, 23, 20, 14, 11, 2,  28, 1,  1,  14, 30, 5,  30]
 [3,  30, 24, 0,  18, 1,  4,  5,  16, 25, 31, 22, 15, 14, 0,  15]
 [31, 15, 3,  17, 4,  26, 0,  11, 24, 8,  8,  6,  3,  28, 0,  16]
 [24, 13, 1,  11, 18, 18, 16, 23, 16, 13, 27, 29, 14, 27, 20, 29]
 [5,  26, 17, 26, 5,  28, 4,  4,  16, 11, 23, 5,  25, 29, 11, 16]
 [30, 26, 18, 7,  3,  11, 10, 12, 15, 5,  28, 11, 11, 22, 20, 5 ]
 [25, 15, 2,  31, 18, 19, 17, 12, 4,  30, 7,  24, 11, 25, 30, 23]
 [13, 29, 1,  13, 27, 5,  9,  30, 6,  25, 25, 24, 30, 24, 24, 0 ]
 [18, 6,  14, 26, 4,  22, 1,  29, 11, 25, 16, 22, 14, 7,  17, 27]
 [30, 13, 7,  19, 20, 24, 7,  4,  11, 0,  12, 29, 0,  24, 23, 4 ]
 [8,  21, 12, 2,  18, 29, 10, 14, 30, 19, 10, 23, 28, 31, 27, 22]
 [8,  2,  28, 19, 5,  27, 26, 21, 25, 19, 11, 16, 8,  15, 11, 11]]
Vector B = [20, 23, 21, 15, 20, 2, 5, 22, 19, 4, 13, 19, 12, 27, 21, 30]
 AA(1) =
[[26, 22, 16, 23, 7,  19, 22, 10, 25, 16, 11, 31, 4,  10, 2, 2]
 [8,  20, 29, 14, 11, 1,  14, 0,  24, 16, 15, 29, 23, 24, 0, 2]]
 BB(1) = [20, 23, 21, 15, 20, 2, 5, 22, 19, 4, 13, 19, 12, 27, 21, 30]
 VV(1) = [3906, 3981]
send result from(1)
 AA(2) =
[[14, 22, 0,  13, 23, 20, 14, 11, 2,  28, 1,  1,  14, 30, 5, 30]
 [3,  30, 24, 0,  18, 1,  4,  5,  16, 25, 31, 22, 15, 14, 0, 15]]
 BB(2) = [20, 23, 21, 15, 20, 2, 5, 22, 19, 4, 13, 19, 12, 27, 21, 30]
 VV(2) = [3958, 3979]
send result from(2)
 AA(3) =
[[31, 15, 3, 17, 4,  26, 0,  11, 24, 8,  8,  6,  3,  28, 0,  16]
 [24, 13, 1, 11, 18, 18, 16, 23, 16, 13, 27, 29, 14, 27, 20, 29]]
 BB(3) = [20, 23, 21, 15, 20, 2, 5, 22, 19, 4, 13, 19, 12, 27, 21, 30]
 VV(3) = [3635, 5392]
send result from(3)
 AA(4) =
[[5,  26, 17, 26, 5, 28, 4,  4,  16, 11, 23, 5,  25, 29, 11, 16]
 [30, 26, 18, 7,  3, 11, 10, 12, 15, 5,  28, 11, 11, 22, 20, 5 ]]
 BB(4) = [20, 23, 21, 15, 20, 2, 5, 22, 19, 4, 13, 19, 12, 27, 21, 30]
 VV(4) = [4245, 4251]
send result from(4)
 AA(5) =
[[25, 15, 2, 31, 18, 19, 17, 12, 4, 30, 7,  24, 11, 25, 30, 23]
 [13, 29, 1, 13, 27, 5,  9,  30, 6, 25, 25, 24, 30, 24, 24, 0 ]]
 BB(5) = [20, 23, 21, 15, 20, 2, 5, 22, 19, 4, 13, 19, 12, 27, 21, 30]
 VV(5) = [4969, 4905]
send result from(5)
 AA(6) =
[[18, 6,  14, 26, 4,  22, 1, 29, 11, 25, 16, 22, 14, 7,  17, 27]
 [30, 13, 7,  19, 20, 24, 7, 4,  11, 0,  12, 29, 0,  24, 23, 4 ]]
 BB(6) = [20, 23, 21, 15, 20, 2, 5, 22, 19, 4, 13, 19, 12, 27, 21, 30]
 VV(6) = [4408, 4069]
send result from(6)RESULT: A*B=[3906, 3981, 3958, 3979, 3635, 5392, 4245, 4251, 4969, 4905, 4408, 4069, 4408, 4069, 5314, 3885]

*/




public class MultiplyMatrixToVector {
    public static void main(String[] args) throws MPIException,
            IOException, ClassNotFoundException {

        Ring ring = new Ring("Z[]");
        MPI.Init(args);
        int rank = MPI.COMM_WORLD.getRank();
        int size = MPI.COMM_WORLD.getSize();
        int ord = 16; // matrix size (ord х ord)
        int k = ord / size; // the number of rows for each processor
        int n = ord - k * (size - 1); // the last portion will be n (less than k) elements )
        if (rank == 0) {
            int den = 10000; // for density=100%
            Random rnd = new Random();
            MatrixS A = new MatrixS(ord, ord, den, new int[]{5},rnd, NumberZ.ONE, ring);
            System.out.println("Matrix A = " + A.toString());
            VectorS B = new VectorS(ord, den, new int[]{5},rnd, ring);
            System.out.println("Vector B = " + B.toString());
            Element[] result = new Element[ord]; // For resulting vector
            for (int j = 1; j < size; j++) {
                Element[][] Mj=new Element[k][];
                int[][] colj=new int[k][];
                for (int s = 0; s < k; s++) {
                    Mj[s]=A.M[s+j*k]; colj[s]=A.col[s+j*k];
                }
                MatrixS Aj=new MatrixS(Mj,colj);
                Transport.sendObject(Aj, j, 1);
                Transport.sendObject(B, j, 2);
            }
            Element[][] M0=new Element[n][];  int[][] col0=new int[n][];
            for (int s = 0; s < n; s++) {
                M0[s]=A.M[s+(size-1)*k]; col0[s]=A.col[s+(size-1)*k];
            }
            MatrixS A0=new MatrixS(M0,col0);
            VectorS V0=A0.multiplyByColumn(B.transpose(ring), ring);
            System.arraycopy(V0.V, 0, result, (size-1)*k, n);
            for (int t = 1; t < size; t++) {
                VectorS Vt = (VectorS) Transport.recvObject(t, t);
                System.arraycopy(Vt.V, 0, result,(t - 1) * k, k);
            }
            System.out.println("RESULT: A*B=" + new VectorS(result).toString(ring));
        } else {
            MatrixS AA = (MatrixS) Transport.recvObject(0, 1 );
            System.out.println(  " AA("+rank+") = " + AA.toString(ring));
            VectorS BB = (VectorS) Transport.recvObject(0, 2 );
            System.out.println(  " BB("+rank+") = " + BB.toString(ring));
            VectorS VV=AA.multiplyByColumn(BB.transpose(ring), ring);
            System.out.println(  " VV("+rank+") = " + VV.toString(ring));
            Transport.sendObject(VV, 0,  rank);
            System.out.println("send result from("+rank+")");
        }
        MPI.Finalize();
    }
}
