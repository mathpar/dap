package com.mathpar.NAUKMA.MAG21.semeniuk;

import com.mathpar.matrix.MatrixS;
import com.mathpar.number.Element;
import com.mathpar.number.NumberZ;
import com.mathpar.number.Ring;
import com.mathpar.number.VectorS;
import mpi.MPI;
import mpi.MPIException;

import java.io.IOException;
import java.util.Random;

/*
Input:
mpirun --allow-run-as-root --hostfile hostfile.txt -np 7 java -cp /DAP/target/classes com/mathpar/NAUKMA/MAG21/semeniuk/MultiplyMatrixToVector

Output:
Matrix A =
[[28, 16, 10, 3,  29, 12, 13, 30, 24, 9,  24, 26, 31, 18, 4,  8 ]
 [11, 19, 12, 14, 3,  21, 22, 9,  12, 29, 8,  2,  17, 2,  9,  15]
 [20, 15, 20, 12, 22, 25, 4,  26, 3,  1,  28, 24, 2,  13, 22, 0 ]
 [27, 2,  15, 4,  9,  6,  17, 10, 2,  20, 0,  4,  4,  0,  9,  19]
 [3,  13, 7,  30, 16, 11, 23, 14, 11, 22, 31, 12, 12, 11, 26, 30]
 [13, 13, 8,  31, 28, 24, 19, 29, 0,  4,  4,  24, 10, 29, 7,  25]
 [30, 1,  27, 15, 2,  27, 31, 16, 6,  31, 25, 15, 20, 5,  9,  25]
 [25, 10, 13, 22, 20, 4,  22, 29, 30, 5,  11, 23, 27, 18, 27, 23]
 [0,  5,  8,  25, 5,  1,  18, 26, 12, 21, 3,  23, 18, 19, 25, 10]
 [22, 18, 24, 1,  8,  2,  16, 31, 20, 14, 12, 24, 14, 23, 29, 7 ]
 [31, 15, 7,  19, 16, 21, 10, 3,  22, 16, 19, 30, 0,  13, 3,  7 ]
 [29, 29, 0,  24, 16, 9,  27, 29, 24, 24, 1,  19, 27, 4,  6,  1 ]
 [19, 25, 14, 22, 28, 14, 30, 4,  28, 7,  2,  28, 19, 11, 0,  10]
 [5,  2,  29, 20, 6,  19, 13, 14, 5,  27, 1,  0,  27, 17, 12, 27]
 [21, 25, 22, 25, 0,  6,  5,  19, 21, 20, 8,  7,  6,  5,  6,  11]
 [7,  27, 6,  12, 31, 22, 0,  11, 17, 15, 4,  24, 6,  21, 9,  16]]
Vector B = [29, 7, 24, 20, 21, 19, 25, 11, 26, 13, 30, 25, 15, 18, 15, 1]
 AA(1) =
[[20, 15, 20, 12, 22, 25, 4,  26, 3, 1,  28, 24, 2, 13, 22, 0 ]
 [27, 2,  15, 4,  9,  6,  17, 10, 2, 20, 0,  4,  4, 0,  9,  19]]
 BB(1) = [29, 7, 24, 20, 21, 19, 25, 11, 26, 13, 30, 25, 15, 18, 15, 1]
 VV(1) = [4853, 2701]
send result from(1)
 AA(2) =
[[3,  13, 7, 30, 16, 11, 23, 14, 11, 22, 31, 12, 12, 11, 26, 30]
 [13, 13, 8, 31, 28, 24, 19, 29, 0,  4,  4,  24, 10, 29, 7,  25]]
 BB(2) = [29, 7, 24, 20, 21, 19, 25, 11, 26, 13, 30, 25, 15, 18, 15, 1]
 VV(2) = [4820, 4692]
send result from(2)
 AA(3) =
[[30, 1,  27, 15, 2,  27, 31, 16, 6,  31, 25, 15, 20, 5,  9,  25]
 [25, 10, 13, 22, 20, 4,  22, 29, 30, 5,  11, 23, 27, 18, 27, 23]]
 BB(3) = [29, 7, 24, 20, 21, 19, 25, 11, 26, 13, 30, 25, 15, 18, 15, 1]
 VV(3) = [5565, 5819]
send result from(3)
 AA(4) =
[[0,  5,  8,  25, 5, 1, 18, 26, 12, 21, 3,  23, 18, 19, 25, 10]
 [22, 18, 24, 1,  8, 2, 16, 31, 20, 14, 12, 24, 14, 23, 29, 7 ]]
 BB(4) = [29, 7, 24, 20, 21, 19, 25, 11, 26, 13, 30, 25, 15, 18, 15, 1]
 VV(4) = [3834, 5035]
send result from(4)
 AA(5) =
[[31, 15, 7, 19, 16, 21, 10, 3,  22, 16, 19, 30, 0,  13, 3, 7]
 [29, 29, 0, 24, 16, 9,  27, 29, 24, 24, 1,  19, 27, 4,  6, 1]]
 BB(5) = [29, 7, 24, 20, 21, 19, 25, 11, 26, 13, 30, 25, 15, 18, 15, 1]
 VV(5) = [4956, 5034]
send result from(5)
 AA(6) =
[[19, 25, 14, 22, 28, 14, 30, 4,  28, 7,  2, 28, 19, 11, 0,  10]
 [5,  2,  29, 20, 6,  19, 13, 14, 5,  27, 1, 0,  27, 17, 12, 27]]
 BB(6) = [29, 7, 24, 20, 21, 19, 25, 11, 26, 13, 30, 25, 15, 18, 15, 1]
 VV(6) = [5222, 3650]
send result from(6)
RESULT: A*B=[4853, 2701, 4820, 4692, 5565, 5819, 3834, 5035, 4956, 5034, 5222, 3650, 5222, 3650, 3762, 3942]
*/

public class MultiplyMatrixToVector {
    public static void main(String[] args) throws MPIException,
            IOException, ClassNotFoundException {

        Ring ring = new Ring("Z[]");
        MPI.Init(args);
        int rank = MPI.COMM_WORLD.getRank();
        int size = MPI.COMM_WORLD.getSize();
        int ord = 16; // matrix size (ord х ord)
        int k = ord / size; // the number of rows for each processor
        int n = ord - k * (size - 1); // the last portion will be n (less than k) elements )
        if (rank == 0) {
            int den = 10000; // for density=100%
            Random rnd = new Random();
            MatrixS A = new MatrixS(ord, ord, den, new int[]{5},rnd, NumberZ.ONE, ring);
            System.out.println("Matrix A = " + A.toString());
            VectorS B = new VectorS(ord, den, new int[]{5},rnd, ring);
            System.out.println("Vector B = " + B.toString());
            Element[] result = new Element[ord]; // For resulting vector
            for (int j = 1; j < size; j++) {
                Element[][] Mj=new Element[k][];
                int[][] colj=new int[k][];
                for (int s = 0; s < k; s++) {
                    Mj[s]=A.M[s+j*k]; colj[s]=A.col[s+j*k];
                }
                MatrixS Aj=new MatrixS(Mj,colj);
                Transport.sendObject(Aj, j, 1);
                Transport.sendObject(B, j, 2);
            }
            Element[][] M0=new Element[n][];  int[][] col0=new int[n][];
            for (int s = 0; s < n; s++) {
                M0[s]=A.M[s+(size-1)*k]; col0[s]=A.col[s+(size-1)*k];
            }
            MatrixS A0=new MatrixS(M0,col0);
            VectorS V0=A0.multiplyByColumn(B.transpose(ring), ring);
            System.arraycopy(V0.V, 0, result, (size-1)*k, n);
            for (int t = 1; t < size; t++) {
                VectorS Vt = (VectorS) Transport.recvObject(t, t);
                System.arraycopy(Vt.V, 0, result,(t - 1) * k, k);
            }
            System.out.println("RESULT: A*B=" + new VectorS(result).toString(ring));
        } else {
            MatrixS AA = (MatrixS) Transport.recvObject(0, 1 );
            System.out.println(  " AA("+rank+") = " + AA.toString(ring));
            VectorS BB = (VectorS) Transport.recvObject(0, 2 );
            System.out.println(  " BB("+rank+") = " + BB.toString(ring));
            VectorS VV=AA.multiplyByColumn(BB.transpose(ring), ring);
            System.out.println(  " VV("+rank+") = " + VV.toString(ring));
            Transport.sendObject(VV, 0,  rank);
            System.out.println("send result from("+rank+")");
        }
        MPI.Finalize();
    }
}
