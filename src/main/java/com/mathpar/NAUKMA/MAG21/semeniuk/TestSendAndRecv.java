package com.mathpar.NAUKMA.MAG21.semeniuk;

import mpi.MPI;
import mpi.MPIException;

import java.util.Random;

/*
Input:
mpirun --allow-run-as-root --hostfile hostfile.txt -np 5 java -cp /DAP/target/classes com/mathpar/NAUKMA/MAG21/semeniuk/TestSendAndRecv

Output:
a[0]= 0.8961358758392963
a[1]= 0.9177122098899165
a[2]= 0.14099709786507642
a[3]= 0.48955549355838424
a[4]= 0.28759602336190804
Proc num 0 ????? ?i?????????

a[0]= 0.8961358758392963
a[1]= 0.9177122098899165
a[2]= 0.14099709786507642
a[3]= 0.48955549355838424
a[4]= 0.28759602336190804
a[0]= 0.8961358758392963
a[1]= 0.9177122098899165
a[2]= 0.14099709786507642
a[3]= 0.48955549355838424
Proc num 1 ????? ????????

a[4]= 0.28759602336190804
Proc num 3 ????? ????????


a[0]= 0.8961358758392963
a[1]= 0.9177122098899165
a[2]= 0.14099709786507642
a[3]= 0.48955549355838424
a[4]= 0.28759602336190804
Proc num 4 ????? ????????

a[0]= 0.8961358758392963
a[1]= 0.9177122098899165
a[2]= 0.14099709786507642
a[3]= 0.48955549355838424
a[4]= 0.28759602336190804
Proc num 2 ????? ????????
*/

public class TestSendAndRecv {
    public static void main(String[] args) throws MPIException {
        //iнiцiалiзацiя MPI
        MPI.Init(args);
        //визначення номера процесора
        int myrank = MPI.COMM_WORLD.getRank();
        //визначення кiлькостi процесорiв у групi
        int np = MPI.COMM_WORLD.getSize();
        //вхiдний параметр - розмiр масиву
        int n = 5;
        double[] a = new double[n];
        //синхронiзацiя процесорiв
        // MPI.COMM_WORLD.barrier();
        // якщо процесор з номером 0
        if (myrank == 0) {
            for (int i = 0; i < n; i++) {
                a[i] = (new Random()).nextDouble();
                System.out.println("a[" + i + "]= " + a[i]);
            }
            //передання 0-процесором елементiв усiм iншим процесорам у групi
            for (int i = 1; i < np; i++) {
                MPI.COMM_WORLD.send(a, n, MPI.DOUBLE, i, 3000);
            }
            System.out.println("Proc num " + myrank + " масив вiдправлено" + "\n");
        } else {
            //приймання i-м процесором повiдомлення вiд
            // процесора з номером 0 та тегом 3000.
            MPI.COMM_WORLD.recv(a, n, MPI.DOUBLE, 0, 3000);
            for (int i = 0; i < n; i++) {
                System.out.println("a[" + i + "]= " + a[i]);
            }
            System.out.println("Proc num " + myrank + " масив прийнято" + "\n");
        }

        // завершення паралельної частини
        MPI.Finalize();
    }
}
