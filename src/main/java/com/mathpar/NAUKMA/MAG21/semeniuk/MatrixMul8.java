package com.mathpar.NAUKMA.MAG21.semeniuk;

import com.mathpar.matrix.MatrixS;
import com.mathpar.number.NumberZ;
import com.mathpar.number.Ring;
import com.mathpar.parallel.utils.MPITransport;
import mpi.MPI;
import mpi.MPIException;

import java.io.IOException;
import java.util.Random;

/*
Input:
mpirun --allow-run-as-root --hostfile hostfile.txt -np 8 java -cp /DAP/target/classes com/mathpar/NAUKMA/MAG21/semeniuk/MatrixMul8

Output:
I'm processor 4
I'm processor 5
I'm processor 7
I'm processor 3
I'm processor 6
I'm processor 2
I'm processor 1
A =
[[11, 10, 28, 20, 8,  27, 1,  29]
 [9,  26, 23, 31, 31, 22, 13, 10]
 [12, 29, 17, 9,  4,  2,  1,  13]
 [3,  14, 16, 26, 14, 11, 29, 16]
 [12, 12, 14, 28, 7,  0,  19, 31]
 [31, 24, 25, 31, 14, 23, 13, 13]
 [2,  26, 19, 22, 23, 8,  6,  14]
 [20, 27, 17, 25, 8,  15, 14, 28]]
B =
[[5,  10, 30, 3,  28, 12, 9,  20]
 [26, 25, 23, 20, 2,  4,  14, 23]
 [11, 30, 14, 14, 29, 0,  11, 12]
 [7,  21, 9,  25, 3,  11, 29, 27]
 [13, 27, 3,  2,  0,  22, 26, 18]
 [18, 19, 8,  22, 18, 22, 10, 2 ]
 [0,  2,  26, 9,  12, 2,  7,  1 ]
 [19, 21, 16, 19, 22, 24, 3,  7 ]]
RES =
[[1904, 2960, 1862, 2295, 2336, 1860, 1699, 1728]
 [2180, 3572, 2236, 2497, 1836, 1985, 2744, 2576]
 [1399, 1965, 1608, 1387, 1248, 805,  1132, 1522]
 [1421, 2387, 2010, 1998, 1552, 1370, 1878, 1691]
 [1402, 2306, 2095, 1946, 1760, 1436, 1650, 1802]
 [2114, 3425, 2883, 2596, 2590, 1961, 2513, 2711]
 [1758, 2781, 1635, 1884, 1249, 1400, 1991, 1994]
 [2070, 3027, 2640, 2467, 2236, 1829, 2010, 2284]]
*/

class MatrixMul8 {
    public static void main(String[] args)throws MPIException, IOException,ClassNotFoundException
    {
        Ring ring = new Ring("Z[]");
        MPI.Init(args);
        int rank = MPI.COMM_WORLD.getRank();
        if (rank == 0) {
            int ord = 8;
            int den = 10000;
            Random rnd = new Random();
            MatrixS A = new MatrixS(ord, ord, den, new int[] {5}, rnd, NumberZ.ONE, ring);
            System.out.println("A = " + A);
            MatrixS B = new MatrixS(ord, ord, den, new int[] {5}, rnd, NumberZ.ONE, ring);
            System.out.println("B = " + B);
            MatrixS D = null;
            MatrixS[] AA = A.split();
            MatrixS[] BB = B.split();
            int tag = 0;
            MPITransport.sendObjectArray(new Object[] {AA[1], BB[2]},0,2, 1, tag);
            MPITransport.sendObjectArray(new Object[] {AA[0], BB[1]},0,2, 2, tag);
            MPITransport.sendObjectArray(new Object[] {AA[1], BB[3]},0,2, 3, tag);
            MPITransport.sendObjectArray(new Object[] {AA[2], BB[0]},0,2, 4, tag);
            MPITransport.sendObjectArray(new Object[] {AA[3], BB[2]},0,2, 5, tag);
            MPITransport.sendObjectArray(new Object[] {AA[2], BB[1]},0,2, 6, tag);
            MPITransport.sendObjectArray(new Object[] {AA[3], BB[3]},0,2, 7, tag);
            MatrixS[] DD = new MatrixS[4];
            DD[0] = (AA[0].multiply(BB[0], ring)).add((MatrixS) MPITransport.recvObject(1, 3),ring);
            DD[1] = (MatrixS) MPITransport.recvObject(2, 3);
            DD[2] = (MatrixS) MPITransport.recvObject(4, 3);
            DD[3] = (MatrixS) MPITransport.recvObject(6, 3);
            D = MatrixS.join(DD);
            System.out.println("RES = " + D.toString());
        }
        else
        {
            System.out.println("I'm processor " + rank);
            Object[] b = new Object[2];
            MPITransport.recvObjectArray(b,0,2,0, 0);
            MatrixS[] a = new MatrixS[b.length];
            for (int i = 0; i < b.length; i++)
                a[i] = (MatrixS) b[i];
            MatrixS res = a[0].multiply(a[1], ring);
            if (rank % 2 == 0)
            {
                MatrixS p = res.add((MatrixS) MPITransport.recvObject(rank + 1, 3), ring);
                MPITransport.sendObject(p, 0, 3);
            }
            else
            {
                MPITransport.sendObject(res, rank - 1, 3);
            }
        }
        MPI.Finalize();
    }
}

