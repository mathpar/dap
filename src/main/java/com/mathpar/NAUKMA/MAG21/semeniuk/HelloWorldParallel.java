package com.mathpar.NAUKMA.MAG21.semeniuk;

import mpi.*;

/*
Input:
mpirun --allow-run-as-root --hostfile hostfile.txt -np 7 java -cp /DAP/target/classes com/mathpar/NAUKMA/MAG21/semeniuk/HelloWorldParallel

Output:
Proc num 1 Hello World
Proc num 5 Hello World
Proc num 3 Hello World
Proc num 0 Hello World
Proc num 2 Hello World
Proc num 4 Hello World
Proc num 6 Hello World
*/

public class HelloWorldParallel {
    public static void main(String[] args) throws MPIException {
        //iнiцiалiзацiя паралельної частини
        MPI.Init(args);
        //визначення номера процесора
        int myrank = MPI.COMM_WORLD.getRank();
        System.out.println("Proc num " + myrank + " Hello World");
        //завершення паралельної частини
        MPI.Finalize();
    }
}