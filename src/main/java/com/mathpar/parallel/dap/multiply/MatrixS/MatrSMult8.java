package com.mathpar.parallel.dap.multiply.MatrixS;

import com.mathpar.log.MpiLogger;
import com.mathpar.matrix.MatrixS;
import com.mathpar.number.Array;
import com.mathpar.number.Element;
import com.mathpar.number.Ring;
import com.mathpar.parallel.dap.core.Drop;
import com.mathpar.parallel.dap.multiply.Multiply;
import com.mathpar.parallel.dap.multiply.MultiplyAdd;

import java.util.ArrayList;

public class MatrSMult8 extends Drop {
    private final static MpiLogger LOGGER = MpiLogger.getLogger(MatrSMult8.class);
    protected static int leafSize = 4;

    //Зв'язки в графі між дропами, перше значення трійки відповідає за номер дропу в графі,
    // друге - за позицію у вихідному масиві(результат який буде прописуватись в інші дропи),
    // третє - за позицію у вхідному масиві дропа (куди буде прописуватись результат)
    private static int[][] _arcs = new int[][] {
            //Зв'язки від вхідної функції до всіх інших дропів
            {1, 0, 0,   1, 4, 1,   2, 1, 0,   2, 6, 1,   3, 0, 0,   3, 5, 1,   4, 1, 0,   4, 7, 1,
                    5, 2, 0,   5, 4, 1,   6, 3, 0,   6, 6, 1,   7, 2, 0,   7, 5, 1,   8, 3, 0,   8, 7, 1},
            {2, 0, 2},//з дропу Множення(дроп 1 в аміні) в дроп номер 2 в цьому аміні, з нульової позиції на другу позицію
            {9, 0, 0},//з дропу Множення з додаванням(дроп 2 в аміні) в дроп номер 9 (масив для вихідної функції), з нульової позиції на нульову позицію
            {4, 0, 2},//з дропу Множення (дроп 3 в аміні) в дроп номер 4 в цьому аміні, з нульової позиції на другу позицію
            {9, 0, 1},//з дропу Множення з додаванням(дроп 4 в аміні) в дроп номер 9 (масив для вихідної функції), з нульової позиції на першу позицію
            {6, 0, 2},//з дропу Множення (дроп 5 в аміні) в дроп номер 6 в цьому аміні, з нульової позиції на другу позицію
            {9, 0, 2},//з дропу Множення з додаванням(дроп 6 в аміні) в дроп номер 9 (масив для вихідної функції), з нульової позиції на другу позицію
            {8, 0, 2},//з дропу Множення (дроп 7 в аміні) в дроп номер 8 в цьому аміні, з нульової позиції на другу позицію
            {9, 0, 3},//з дропу Множення з додаванням(дроп 8 в аміні) в дроп номер 9 (масив для вихідної функції), з нульової позиції на третю позицію
            {}};//відповідає за вихідну функцію, тому пустий

    public MatrSMult8() {
        //Має 2 вхідні матриці та 1 вихідну
        inData = new Element[2];
        outData = new Element[1];
        //Немає додаткових компонентів
        numberOfMainComponents = 2;
        numberOfMainComponentsAtOutput = 16;
        //Дроп множення має тип 21
        type = 21;
        //кількість блоків, для формування результату
        resultForOutFunctionLength = 4;
        inputDataLength = 2;
        //унікальний номер дропа
        number = cnum++;
        arcs = _arcs;
    }

    //Розгортання аміну з дропами, відповідно до графу, для обрахунку поточного дропа.
    @Override
    public ArrayList<Drop> doAmin() {
        ArrayList<Drop> amin = new ArrayList<Drop>();

        amin.add(new MatrSMult8());
        amin.add(new MatrSMultiplyAdd());
        amin.add(new MatrSMult8());
        amin.add(new MatrSMultiplyAdd());
        amin.add(new MatrSMult8());
        amin.add(new MatrSMultiplyAdd());
        amin.add(new MatrSMult8());
        amin.add(new MatrSMultiplyAdd());

        return amin;
    }

    //Послідовний обрахунок листових вершин
    @Override
    public void sequentialCalc(Ring ring) {
        // LOGGER.info("in sequentialCalc indata = " + inData[0] + ",  "+inData[1]);
        MatrixS A =  (MatrixS)inData[0];
        MatrixS B =  (MatrixS)inData[1];
        MatrixS C = A.multiply(B, ring);

        if (key == 0) outData[0] = C;
        else
            outData[0] = C.negate(ring);
    }

    @Override
    //Вхідна функція дропа, розбиває вхідні дані на блоки.
    public MatrixS[] inputFunction(Element[] input, int inputKey, Ring ring) {

        MatrixS[] res = new MatrixS[8];
        MatrixS ms = (MatrixS) input[0];
        MatrixS ms1 = (MatrixS) input[1];
        Array.concatTwoArrays(ms.split(), ms1.split(), res);
        return res;

    }

    //Вихідна функція дропа, яка збирає блоки в результат
    @Override
    public MatrixS[] outputFunction(Element[] input, Ring ring) {
        MatrixS[] resmat = new MatrixS[input.length];
        for (int i = 0; i < input.length; i++) {
            resmat[i] = (MatrixS) input[i];
        }
        MatrixS[] res;
        if(key == 0) res = new MatrixS[] {MatrixS.join(resmat)};
        else
            res = new MatrixS[] {MatrixS.join(resmat).negate(ring)};

        return res;
    }
    //Перевіряє чи є дроп листовим
    @Override
    public boolean isItLeaf() {
        MatrixS ms = (MatrixS) inData[0];
        return (ms.size <= leafSize);
    }

    @Override
    public void setLeafSize(int dataSize) {
        leafSize = dataSize;
    }
}
