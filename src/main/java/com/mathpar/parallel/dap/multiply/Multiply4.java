package com.mathpar.parallel.dap.multiply;

import com.mathpar.log.MpiLogger;
import com.mathpar.matrix.MatrixS;
import com.mathpar.number.Array;
import com.mathpar.number.Element;
import com.mathpar.number.Ring;
import com.mathpar.parallel.dap.core.Drop;

import java.util.ArrayList;

public class Multiply4 extends Drop {
    private final static MpiLogger LOGGER = MpiLogger.getLogger(Multiply4.class);
    protected static int leafSize = 4;


    private static int[][] _arcs = new int[][] {
            {1, 0, 0,   1, 4, 1,   1, 1, 2,   1, 6, 3,   2, 0, 0,   2, 5, 1,   2, 1, 2,   2, 7, 3,
                    3, 2, 0,   3, 4, 1,   3, 3, 2,   3, 6, 3,   4, 2, 0,   4, 5, 1,   4, 3, 2,   4, 7, 3},
            {5, 0, 0},
            {5, 0, 1},
            {5, 0, 2},
            {5, 0, 3},
            {}};

    public Multiply4() {
        //Має 2 вхідні матриці та 1 вихідну
        inData = new Element[2];
        outData = new Element[1];
        //Немає додаткових компонентів
        numberOfMainComponents = 2;
        numberOfMainComponentsAtOutput = 16;
        //Дроп має тип 17
        type = 17;
        //кількість блоків, для формування результату
        resultForOutFunctionLength = 4;
        inputDataLength = 2;
        //унікальний номер дропа
        number = cnum++;
        arcs = _arcs;
    }

    //Розгортання аміну з дропами, відповідно до графу, для обрахунку поточного дропа.
    @Override
    public ArrayList<Drop> doAmin() {
        ArrayList<Drop> amin = new ArrayList<Drop>();

        amin.add(new MultiplyScalar());
        amin.add(new MultiplyScalar());
        amin.add(new MultiplyScalar());
        amin.add(new MultiplyScalar());

        return amin;
    }

    //Послідовний обрахунок листових вершин
    @Override
    public void sequentialCalc(Ring ring) {
        // LOGGER.info("in sequentialCalc indata = " + inData[0] + ",  "+inData[1]);
        MatrixS A =  (MatrixS)inData[0];
        MatrixS B =  (MatrixS)inData[1];
        MatrixS C = A.multiply(B, ring);

        if(key == 0) outData[0] = C;
        else
            outData[0] = C.negate(ring);
    }

    @Override
    //Вхідна функція дропа, розбиває вхідні дані на блоки.
    public MatrixS[] inputFunction(Element[] input, int inputKey, Ring ring) {

        MatrixS[] res = new MatrixS[8];
        MatrixS ms = (MatrixS) input[0];
        MatrixS ms1 = (MatrixS) input[1];
        Array.concatTwoArrays(ms.split(), ms1.split(), res);
        return res;

    }

    //Вихідна функція дропа, яка збирає блоки в результат
    @Override
    public MatrixS[] outputFunction(Element[] input, Ring ring) {
        MatrixS[] resmat = new MatrixS[input.length];
        for (int i = 0; i < input.length; i++) {
            resmat[i] = (MatrixS) input[i];
        }

        MatrixS[] res;
        if(key == 0) res = new MatrixS[] {MatrixS.join(resmat)};
        else
        res = new MatrixS[] {MatrixS.join(resmat).negate(ring)};

        LOGGER.info("out key = " + key);
        return res;
    }
    //Перевіряє чи є дроп листовим
    @Override
    public boolean isItLeaf() {
        MatrixS ms = (MatrixS) inData[0];
        return (ms.size <= leafSize);
    }

    @Override
    public void setLeafSize(int dataSize) {
        leafSize = dataSize;
    }

}
