package com.mathpar.parallel.dap.multiply.MatrixD;

import com.mathpar.log.MpiLogger;
import com.mathpar.matrix.MatrixD;
import com.mathpar.matrix.MatrixS;
import com.mathpar.number.Array;
import com.mathpar.number.Element;
import com.mathpar.number.Ring;
import com.mathpar.parallel.dap.core.Drop;
import com.mathpar.parallel.dap.multiply.MatrixS.MatrSMult8;
import com.mathpar.parallel.dap.multiply.MatrixS.MatrSMultiplyAdd;

import java.util.ArrayList;

public class MatrDMult8 extends Drop {
    private final static MpiLogger LOGGER = MpiLogger.getLogger(MatrSMult8.class);
    protected static int leafSize = 4;

    //Зв'язки в графі між дропами, перше значення трійки відповідає за номер дропу в графі,
    // друге - за позицію у вихідному масиві(результат який буде прописуватись в інші дропи),
    // третє - за позицію у вхідному масиві дропа (куди буде прописуватись результат)
    private static int[][] _arcs = new int[][] {
            //Зв'язки від вхідної функції до всіх інших дропів
            {1, 0, 0,   1, 4, 1,   2, 1, 0,   2, 6, 1,   3, 0, 0,   3, 5, 1,   4, 1, 0,   4, 7, 1,
                    5, 2, 0,   5, 4, 1,   6, 3, 0,   6, 6, 1,   7, 2, 0,   7, 5, 1,   8, 3, 0,   8, 7, 1},
            {2, 0, 2},//з дропу Множення(дроп 1 в аміні) в дроп номер 2 в цьому аміні, з нульової позиції на другу позицію
            {9, 0, 0},//з дропу Множення з додаванням(дроп 2 в аміні) в дроп номер 9 (масив для вихідної функції), з нульової позиції на нульову позицію
            {4, 0, 2},//з дропу Множення (дроп 3 в аміні) в дроп номер 4 в цьому аміні, з нульової позиції на другу позицію
            {9, 0, 1},//з дропу Множення з додаванням(дроп 4 в аміні) в дроп номер 9 (масив для вихідної функції), з нульової позиції на першу позицію
            {6, 0, 2},//з дропу Множення (дроп 5 в аміні) в дроп номер 6 в цьому аміні, з нульової позиції на другу позицію
            {9, 0, 2},//з дропу Множення з додаванням(дроп 6 в аміні) в дроп номер 9 (масив для вихідної функції), з нульової позиції на другу позицію
            {8, 0, 2},//з дропу Множення (дроп 7 в аміні) в дроп номер 8 в цьому аміні, з нульової позиції на другу позицію
            {9, 0, 3},//з дропу Множення з додаванням(дроп 8 в аміні) в дроп номер 9 (масив для вихідної функції), з нульової позиції на третю позицію
            {}};//відповідає за вихідну функцію, тому пустий

    public MatrDMult8() {
        //Має 2 вхідні матриці та 1 вихідну
        inData = new Element[2];
        outData = new Element[1];
        //Немає додаткових компонентів
        numberOfMainComponents = 2;
        numberOfMainComponentsAtOutput = 16;
        //Дроп множення має тип 29
        type = 29;
        //кількість блоків, для формування результату
        resultForOutFunctionLength = 4;
        inputDataLength = 2;
        //унікальний номер дропа
        number = cnum++;
        arcs = _arcs;
    }

    //Розгортання аміну з дропами, відповідно до графу, для обрахунку поточного дропа.
    @Override
    public ArrayList<Drop> doAmin() {
        ArrayList<Drop> amin = new ArrayList<Drop>();

        amin.add(new MatrDMult8());
        amin.add(new MatrDMultiplyAdd());
        amin.add(new MatrDMult8());
        amin.add(new MatrDMultiplyAdd());
        amin.add(new MatrDMult8());
        amin.add(new MatrDMultiplyAdd());
        amin.add(new MatrDMult8());
        amin.add(new MatrDMultiplyAdd());

        return amin;
    }

    //Послідовний обрахунок листових вершин
    @Override
    public void sequentialCalc(Ring ring) {
        // LOGGER.info("in sequentialCalc indata = " + inData[0] + ",  "+inData[1]);
        MatrixD A =  (MatrixD)inData[0];
        MatrixD B =  (MatrixD)inData[1];
        MatrixD C = A.multiplyMatr(B, ring);

        if (key == 0) outData[0] = C;
        else
            outData[0] = C.negate(ring);
    }

    @Override
    //Вхідна функція дропа, розбиває вхідні дані на блоки.
    public MatrixD[] inputFunction(Element[] input, int inputKey, Ring ring) {

        MatrixD[] res = new MatrixD[8];
        MatrixD ms = (MatrixD) input[0];
        MatrixD ms1 = (MatrixD) input[1];
        Array.concatTwoArrays(ms.splitTo4(), ms1.splitTo4(), res);
        return res;

    }

    //Вихідна функція дропа, яка збирає блоки в результат
    @Override
    public MatrixD[] outputFunction(Element[] input, Ring ring) {
        MatrixD[] resmat = new MatrixD[input.length];
        for (int i = 0; i < input.length; i++) {
            resmat[i] = (MatrixD) input[i];
        }

        MatrixD[] res;
        if(key == 0) res = new MatrixD[] {MatrixD.join(resmat)};
        else
            res = new MatrixD[] {MatrixD.join(resmat).negate(ring)};
        return res;
    }
    //Перевіряє чи є дроп листовим
    @Override
    public boolean isItLeaf() {
        MatrixD ms = (MatrixD) inData[0];
        return (ms.M.length <= leafSize);
    }

    @Override
    public void setLeafSize(int dataSize) {
        leafSize = dataSize;
    }
}
