package com.mathpar.parallel.dap.multiply.MatrixD;

import com.mathpar.log.MpiLogger;
import com.mathpar.matrix.MatrixD;
import com.mathpar.matrix.MatrixS;
import com.mathpar.number.Array;
import com.mathpar.number.Element;
import com.mathpar.number.Ring;
import com.mathpar.parallel.dap.multiply.MatrixS.MatrSMult8;
import com.mathpar.parallel.dap.multiply.MultiplyAdd;

public class MatrDMultiplyAdd extends MatrDMult8 {
    private final static MpiLogger LOGGER = MpiLogger.getLogger(MatrDMultiplyAdd.class);

    private int[][] _arcs = new int[][] {{1, 0, 0, 1, 4, 1, 2, 1, 0, 2, 6, 1, 3, 0, 0, 3, 5, 1, 4, 1, 0, 4, 7, 1,
            5, 2, 0, 5, 4, 1, 6, 3, 0, 6, 6, 1, 7, 2, 0, 7, 5, 1, 8, 3, 0, 8, 7, 1, 9, 8, 4},
            {2, 0, 2}, {9, 0, 0}, {4, 0, 2}, {9, 0, 1}, {6, 0, 2}, {9, 0, 2}, {8, 0, 2}, {9, 0, 3}, {}};

    public MatrDMultiplyAdd() {
        inData = new Element[3];
        outData = new Element[1];
        numberOfMainComponents = 2;
        numberOfMainComponentsAtOutput = 16;
        type = 31;
        resultForOutFunctionLength = 5;
        inputDataLength = 3;
        number = cnum++;
        arcs = _arcs;
    }

    @Override
    public void sequentialCalc(Ring ring) {
        // LOGGER.info("in sequentialCalc indata = " + inData[0] );//+ ",  "+inData[1] +"inData[2] = " + inData[2]);
        //LOGGER.info("sequentialCalc MAdd, inData[2] = " + inData[2]);
        outData[0] = ((MatrixD)inData[0]).multiplyMatr((MatrixD)inData[1], ring).add((MatrixD)inData[2], ring);
    }

    @Override
    //input key : 1 - full, 2 - main, 3- notmain
    public MatrixD[] inputFunction(Element[] input, int inputKey, Ring ring) {

        MatrixD[] res = new MatrixD[9];
        //LOGGER.info("inputFunction inputKey = " + inputKey + " res.length = " + res.length);
        if (inputKey != 3) {
            MatrixD ms = (MatrixD) input[0];
            MatrixD ms1 = (MatrixD) input[1];
            //  LOGGER.info("ms = " + inputKey + " ms1 = " + res.length);
            Array.concatTwoArrays(ms.splitTo4(), ms1.splitTo4(), res);

            if (inputKey == 2) {
                res[8] = null;
            }
            if (inputKey == 1) {
                res[8] = (MatrixD) input[2];
            }
        }
        if (inputKey == 3) {
            res[8] = (MatrixD) input[0];
        }
        return res;

    }

    @Override
    public MatrixD[] outputFunction(Element[] input, Ring ring) {

        MatrixD[] resmat = new MatrixD[input.length];
        for (int i = 0; i < input.length; i++) {
            resmat[i] = (MatrixD) input[i];
        }
        MatrixD[] res = new MatrixD[] {MatrixD.join(resmat).add((MatrixD) input[4], ring)};

        return res;
    }

}
