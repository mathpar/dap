package com.mathpar.parallel.dap.multiply.MatrixS;

import com.mathpar.log.MpiLogger;
import com.mathpar.matrix.MatrixS;
import com.mathpar.number.Array;
import com.mathpar.number.Element;
import com.mathpar.number.Ring;
import com.mathpar.parallel.dap.multiply.Multiply;

public class MultiplyExtendedS8 extends MatrSMult8 {
    private final static MpiLogger LOGGER = MpiLogger.getLogger(MultiplyExtendedS8.class);
    private static int[][] _arcs = new int[][] {{1, 0, 0, 1, 4, 1, 2, 1, 0, 2, 6, 1, 3, 0, 0, 3, 5, 1, 4, 1, 0, 4, 7, 1,
            5, 2, 0, 5, 4, 1, 6, 3, 0, 6, 6, 1, 7, 2, 0, 7, 5, 1, 8, 3, 0, 8, 7, 1, 9, 8, 4, 9, 9, 5},
            {2, 0, 2}, {9, 0, 0}, {4, 0, 2}, {9, 0, 1}, {6, 0, 2}, {9, 0, 2}, {8, 0, 2}, {9, 0, 3}, {}};
    public MultiplyExtendedS8() {
        inData = new Element[2];
        outData = new Element[2];
        numberOfMainComponents = 1;
        arcs = _arcs;
        type = 26;
        resultForOutFunctionLength = 6;
        inputDataLength = 2;
        numberOfMainComponentsAtOutput = 17;
        number = cnum++;
    }

    @Override
    public void sequentialCalc(Ring ring) {
        // LOGGER.info("in sequentialCalc indata = " + inData[0] + ",  "+inData[1]);
        MatrixS b = ((MatrixS) inData[0]).transpose();
        // LOGGER.info("b = " +b);
        outData[1] = b;
        MatrixS bbT =  b.multiply((MatrixS) inData[0], ring);
        // LOGGER.info("bbT= " + bbT);
        outData[0] = ((MatrixS)inData[1]).subtract(bbT, ring);
        // LOGGER.info(" outData[0] = " + outData[0]);
    }

    @Override
    //input key : 1 - full, 2 - main, 3- notmain
    public MatrixS[] inputFunction(Element[] input, int inputKey, Ring ring) {

        MatrixS[] res = new MatrixS[10];
        if (inputKey != 3) {
            MatrixS ms = ((MatrixS) input[0]).transpose();
            MatrixS ms1 = (MatrixS) input[0];

            Array.concatTwoArrays(ms.split(), ms1.split(), res);
            res[8] = ms;

            if (inputKey == 2) {
                res[9] = null;
            }
            if (inputKey == 1) {
                res[9] = (MatrixS) input[1];
            }

           /* for (MatrixS m : res) {
                LOGGER.info(m.toString());
            }*/
        }
        if (inputKey == 3) {
            res[9] = (MatrixS) input[0];
        }
        return res;
    }

    @Override
    public MatrixS[] outputFunction(Element[] input, Ring ring) {

        //LOGGER.info("input length = " + input.length);
        MatrixS delta = (MatrixS)input[5];
        MatrixS b = (MatrixS)input[4];
        MatrixS[] resmat = new MatrixS[input.length];
        for (int i = 0; i < input.length; i++) {
            resmat[i] = (MatrixS) input[i];
        }
        MatrixS[] res = new MatrixS[] {delta.add(MatrixS.join(resmat).negate(ring),ring), b};
        // LOGGER.info("res in outputFunction = " + res[0]);
        return res;

    }
}
