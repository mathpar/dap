/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
//package com.mathpar.students.ukma17i41.bosa.parallel.engine;
package com.mathpar.parallel.dap.core;

import com.mathpar.number.Element;

import java.util.ArrayList;

public class Amin {
    int parentProc;
    int parentAmin;
    int parentDrop;
    int type;
    byte[] config;
    int aminIdInPine;
    int recNumb;
    int key;
    ArrayList<Drop> branch;

    Element[] inputData;
    Element[] outputData;
    Element[] resultForOutFunction;

    public Amin(int type, int key, byte[] config, int pProc, int pAmin, int dropNum, int index, int myRank, int recNum) {

        Drop drop = Drop.getDropObject(type, config);

        this.type = type;
        this.config = config;
        this.key = key;
        branch = drop.doAmin();
        setIndexToDrops(index, myRank, recNum);
        inputData = new Element[drop.inputDataLength];
        resultForOutFunction = new Element[drop.resultForOutFunctionLength];
        outputData = null;
        parentProc = pProc;
        parentAmin = pAmin;
        parentDrop = dropNum;
        recNumb = recNum;
        aminIdInPine = index;

    }

    private void setIndexToDrops(int index, int myRank, int recNum) {
        for (int i = 0; i < branch.size(); i++) {
            branch.get(i).aminId = index;
            branch.get(i).dropId = i;
            branch.get(i).procId = myRank;
            branch.get(i).recNum = recNum;
            
        }
    }

    @Override
    public String toString() {
        String str = "";
        for (Drop i : branch) {
            str += i + "\n";
        }

        return str;
    }

    public boolean hasFullOutput(){
        for (int j = 0; j < resultForOutFunction.length; j++) {
            if (resultForOutFunction[j] == null) {
                return false;
            }
        }

        return true;
    }

    //public int GetMyDrop(Drop dp) {
    //    return branch.indexOf(dp);
    //}

}
