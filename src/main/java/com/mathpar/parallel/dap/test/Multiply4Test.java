package com.mathpar.parallel.dap.test;

import com.mathpar.log.MpiLogger;
import com.mathpar.matrix.MatrixS;
import com.mathpar.number.Element;
import com.mathpar.number.Ring;
import com.mathpar.parallel.dap.core.DispThread;
import mpi.MPIException;
import org.javatuples.Pair;

import java.io.IOException;
import java.util.Random;

public class Multiply4Test extends DAPTest{
    private final static MpiLogger LOGGER = MpiLogger.getLogger(Multiply4Test.class);
    protected Multiply4Test() {
        super("multiply4", 17, 1);
        ring = new Ring("R[]");
    }

    @Override
    protected MatrixS[] initData(int size, int density, int maxBits, Ring ring) {
        return new MatrixS[]{matrix(size, density, maxBits, ring), matrix(size, density, maxBits, ring)};
    }

    @Override
    protected Pair<Boolean, Element> checkResult(DispThread dispThread, String[] args, Element[] initData, Element[] resultData, Ring ring) {
        MatrixS matrixA = (MatrixS) initData[0];
        // LOGGER.trace("matrixA = "+matrixA);
        MatrixS matrixB = (MatrixS) initData[1];
        // LOGGER.trace("matrixB = "+matrixB);
        MatrixS res = (MatrixS) resultData[0];
        //  LOGGER.trace("res = "+res.toString(ring));

        MatrixS check = matrixA.multiply(matrixB, ring);

        check = check.subtract(res, ring);
        boolean succeed = check.isZero(ring);

        Element element = check.max(ring);

        return new Pair<>(succeed, element);
    }

    @Override
    protected int dispRunsOnOtherProc() {
        return 0;
    }

    @Override
    protected MatrixS matrix(int size, int density, int maxBits, Ring ring){
        MatrixS matrix = new MatrixS(size, size, density, new int[]{maxBits}, new Random(),ring.numberONE(), ring);
        // LOGGER.trace("bef matrix = " + matrix);
      /*  for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                if(!matrix.getElement(i,j, ring).equals(ring.numberZERO))
                    matrix.putElement( ring.numberONE.divide(matrix.getElement(i,j, ring),  ring), i, j);
            }
        }*/
        LOGGER.info("matrix = " + matrix);
        return matrix;
    }

    @Override
    protected Element[] sequentialExecute(Element[] data, Ring ring) {
        return new Element[] {data[0].multiply(data[1],ring)};
    }

    public static void main(String[] args) throws InterruptedException, ClassNotFoundException, MPIException, IOException {
        Multiply4Test test = new Multiply4Test();
        test.runTests(args);
    }
}
