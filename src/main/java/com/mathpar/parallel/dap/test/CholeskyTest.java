/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mathpar.parallel.dap.test;

import com.mathpar.log.MpiLogger;
import com.mathpar.matrix.MatrixS;
import com.mathpar.number.Element;
import com.mathpar.number.Ring;
import com.mathpar.parallel.dap.core.DispThread;
import mpi.MPIException;
import org.javatuples.Pair;

import java.util.Random;

/**
 * @author alla
 *
 * mpirun --hostfile /home/alla/hostfile -np 4 java -cp /home/alla/openmpi/lib/mpi.jar:target/qr-test.jar -Xmx4g com.mathpar.parallel.dap.test.CholeskyTest -size=64 -leaf=16  -density=100 -nocheck > debug.log
 */
public class CholeskyTest extends DAPTest {
  private final static MpiLogger LOGGER = MpiLogger.getLogger(CholeskyTest.class);
  MatrixS initMatrix;

    protected CholeskyTest() {
        super("cholesky_test", 5, 0);
       // ring = new Ring("R[]");
        //ring.setAccuracy(180);
       // ring.setMachineEpsilonR(160);
        //;
        //ring.setFLOATPOS(190);
    }

    @Override
    protected MatrixS[] initData(int size, int density, int maxBits, Ring ring) {
        return new MatrixS[]{matrix(size, density, maxBits, ring)};
    }

    @Override
    protected Pair<Boolean, Element> checkResult(DispThread dispThread, String[] args, Element[] initData, Element[] resultData, Ring ring) {
        //MatrixS matrix = (MatrixS) initData[0];
        MatrixS L = (MatrixS) resultData[0];
        //MatrixS Linv = (MatrixS) resultData[1];

      //  MatrixS LLT = L.multiply(L.transpose(), ring);

        //LOGGER.trace("initMatrix = "+initMatrix.multiply(initMatrix.transpose(), ring));
       // LOGGER.trace("L*L^T = "+L.multiply(L.transpose(), ring));
        MatrixS check = initMatrix.subtract(L, ring);
//        Element[] resultCheck = runTask(dispThread, 0, args, new Element[]{L, L.transpose()}, ring);
//        MatrixS check = (MatrixS) resultCheck[0];
        boolean succeed = check.isZero(ring);

        Element element = check.max(ring);

        return new Pair<>(succeed, element);
    }

    public static void main(String[] args) throws InterruptedException, MPIException {
        CholeskyTest test = new CholeskyTest();
        test.runTests(args);
        LOGGER.info("end test");
    }

	@Override
    protected MatrixS matrix(int size, int density, int maxBits, Ring ring){
        MatrixS matrix = new MatrixS(size, size, density, new int[] {maxBits}, new Random(), ring.numberONE(), ring);
        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                if (i < j) {
                    matrix.putElement(ring.numberZERO, i, j);
                }
                else
                if (i == j) {
                    if (matrix.getElement(i, j, ring).isZero(ring)) {
                        matrix.putElement(ring.numberONE, i, j);
                    }
                }
            }
        }

        initMatrix = matrix;
        MatrixS res = matrix.multiply(matrix.transpose(), ring);

        return res;
    }

    @Override
    protected Element[] sequentialExecute(Element[] data, Ring ring) {
        return ((MatrixS) data[0]).choleskyFactorize(ring);
    }
}
